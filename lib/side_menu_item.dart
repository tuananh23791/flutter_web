import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


import 'constants.dart';
import 'counter_badge.dart';
//
// class SideMenuItem extends StatelessWidget {
//   const SideMenuItem({
//     Key key,
//     this.isActive,
//     this.isHover = false,
//     this.itemCount,
//     this.showBorder = true,
//     @required this.iconSrc,
//     @required this.title,
//     @required this.press,
//   }) : super(key: key);
//
//   final bool isActive, isHover, showBorder;
//   final int itemCount;
//   final String iconSrc, title;
//   final VoidCallback press;
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: kDefaultPadding),
//       child: InkWell(
//         onTap: press,
//         child: Row(
//           children: [
//             SizedBox(width: kDefaultPadding / 4),
//             Expanded(
//               child: Container(
//                 padding: EdgeInsets.only(bottom: 15, right: 5),
//                 decoration: showBorder
//                     ? BoxDecoration(
//                         border: Border(
//                           bottom: BorderSide(color: Color(0xFFDFE2EF)),
//                         ),
//                       )
//                     : null,
//                 child: Row(
//                   children: [
//                     Icon(Icons.ac_unit, color: (isActive || isHover) ? kPrimaryColor : kGrayColor,),
//                     SizedBox(width: kDefaultPadding * 0.75),
//                     Text(
//                       title,
//                       style: Theme.of(context).textTheme.button.copyWith(
//                             color:
//                                 (isActive || isHover) ? kTextColor : kGrayColor,
//                           ),
//                     ),
//                     Spacer(),
//                     if (itemCount != null) CounterBadge(count: itemCount)
//                   ],
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
//

//
// class SideMenuItem extends StatelessWidget {
//   const SideMenuItem({
//     Key key,
//     @required this.isActive = false,
//     @required this.isHover = false,
//     this.itemCount,
//     this.showBorder = true,
//     @required this.title,
//     @required this.press,
//   }) : super(key: key);
//
//   final bool isActive, isHover, showBorder;
//   final int itemCount;
//   final String title;
//   final VoidCallback press;
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: kDefaultPadding),
//       child: InkWell(
//         onTap: press,
//         child: Row(
//           children: [
//             Container(
//               padding: EdgeInsets.only(bottom: 15, right: 5),
//               decoration: showBorder
//                   ? BoxDecoration(
//                 border: Border(
//                   bottom: BorderSide(color: Color(0xFFDFE2EF)),
//                 ),
//               )
//                   : null,
//               child: Row(
//                 children: [
//                   Icon(Icons.ac_unit, color: (isActive || isHover) ? kPrimaryColor : kGrayColor,),
//                   // SizedBox(width: kDefaultPadding * 0.75),
//                   Text(
//                     title,
//                     style: Theme.of(context).textTheme.button.copyWith(
//                       color:
//                       (isActive || isHover) ? kTextColor : kGrayColor,
//                     ),
//                   ),
//                   // Spacer(),
//                   // if (itemCount != null) CounterBadge(count: itemCount)
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

class SideMenuItem extends StatelessWidget {
//isActive
  final String titleString;
  final bool isActive;
  final bool isHover;
  final VoidCallback press;

  SideMenuItem({
    Key key,
     this.isHover = false, @required this.press,  this.isActive = false, @required this.titleString,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: (isHover || isActive) ? Colors.yellow : Colors.white,
      shape: ContinuousRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      borderOnForeground: true,
      elevation: 0,
      margin: EdgeInsets.fromLTRB(0,0,0,0),
      child: ListTile(
        onTap: press,
        mouseCursor: SystemMouseCursors.click,
        leading: Icon(Icons.accessibility),
        title: Text(titleString, style: TextStyle(fontSize: 12),),
      ),
    );
  }
}
