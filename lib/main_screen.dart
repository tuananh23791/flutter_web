import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/size_config.dart';
import 'package:web_dashboard_admin/widget/right_side_content/right_side_content.dart';
import 'package:web_dashboard_admin/widget/slide_menu/item_slide_menu_controller.dart';

import 'auth_dialog.dart';
import 'responsive.dart';
import 'side_menu.dart';


class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    Get.put(ItemSlideMenuController());
    // It provide us the width and height
    return Scaffold(
      //dùng kiểu này xác định case cho 3 case !
      body: Responsive(
        // Let's work on our mobile part
        mobile: RightSideContent(),
        tablet: Row(
          children: [
            Container(
                // child: SideMenu()
            ),
            ///khong nen xoa, để làm vi du
            // Expanded(
            //   flex: 6,
            //   child: Container(color: Colors.blue,),
            // ),
            Expanded(
              // flex: 9,
              child: RightSideContent(),
            ),
          ],
        ),
        desktop: Row(
          children: [
            // Once our width is less then 1300 then it start showing errors
            // Now there is no error if our width is less then 1340
            Container(
              // width: 200.0,
                child: SideMenu()
            ),
            ///khong nen xoa, để làm vi du
            // Expanded(
            //   flex: 6,
            //   child: Container(color: Colors.blue,),
            // ),
            Expanded(
              // flex: 9,
              child: RightSideContent(),
            ),
          ],
        ),
      ),
    );
  }
}


// class RightSideContent extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//
//         drawer: Responsive.isMobile(context) ? Drawer( //dùg để check 1 case cụ thể nào đó nếu thoả
//           child: SideMenu(),
//         ) : null,
//         appBar: AppBar(
//           title: Obx(()=> Text(Get.find<ItemSlideMenuController>().title.value)),
//           actions: [
//             IconButton(icon: Icon(Icons.login), onPressed: (){
//               print("clicked");
//               showPopupLogin(context);
//             })
//           ],
//         ),
//         body: Container(color: Colors.green,)
//     );
//   }
//
//   void showPopupLogin(BuildContext context){
//     showDialog(
//       context: context,
//       builder: (context) => AuthDialog(),
//     );
//   }
//
// }
