import 'package:flutter/material.dart';

class Utils {
  static final Utils _singleton = Utils._internal();

  factory Utils() {
    return _singleton;
  }

  Utils._internal();

  static Utils get shared => _singleton;

  dialog(BuildContext context, Widget widget) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return widget;
      },
    );
  }

  changeScreen(BuildContext context, Widget page) {
    Navigator.of(context).push(MaterialPageRoute(
      // fullscreenDialog: true,
      builder: (context) => page,
    ));
  }

  changeScreenAndRemove(BuildContext context, Widget page) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          fullscreenDialog: true,
          builder: (context) => page,
        ),
        (Route<dynamic> route) => false);
  }
  pop(BuildContext context) {
    Navigator.of(context).pop();
  }
}
