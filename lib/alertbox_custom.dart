import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class AlertPopupOneButton extends StatelessWidget {
  final Function() callbackAfterPressOK;
  final String title;
  final String content;
  const AlertPopupOneButton({Key key, this.callbackAfterPressOK, this.title, this.content}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text("OK"),
          onPressed: (){
            if (callbackAfterPressOK != null) {
              callbackAfterPressOK();
            }
          },
        ),
      ],
    );
  }
}

//
class AlertPopup2Button extends StatelessWidget {
  final Function() callbackAfterPressOK;
  final String titleButtonRight;
  final String titleButtonLeft;
  final String title;
  final String content;
  const AlertPopup2Button({Key key, this.callbackAfterPressOK, this.title, this.content, this.titleButtonRight = 'OK', this.titleButtonLeft = 'Cancel'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text(titleButtonLeft),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),

        CupertinoDialogAction(
          isDestructiveAction: true,
          isDefaultAction: false,
          child: Text(titleButtonRight),
          onPressed: (){
            callbackAfterPressOK();
          },
        ),
      ],
    );
  }
}
//
//
//
// class AlertPopupWithTextField extends StatelessWidget {
//   var texfieldController = TextEditingController();
//   final Function(String) callbackAfterPressOK;
//   final String confirmText;
//   final String cancleText;
//   final String title;
//   final String placeholder;
//   final bool obscureText;
//   AlertPopupWithTextField({Key key, this.callbackAfterPressOK, this.confirmText, this.cancleText, this.title, this.placeholder, this.obscureText}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return CupertinoAlertDialog(
//       title: Padding(
//         padding: const EdgeInsets.all(15.0),
//         child: Text(title, style: Styles.mclub_normalText,),
//       ),
//       content: CupertinoTextField(placeholder: placeholder, obscureText: obscureText, controller: texfieldController,),
//       actions: [
//         CupertinoDialogAction(
//           isDefaultAction: true,
//           child: Text(cancleText),
//           onPressed: (){
//             Get.back();
//           },
//         ),
//         CupertinoDialogAction(
//           isDefaultAction: true,
//           child: Text(confirmText),
//           onPressed: (){
//             Get.back();
//             callbackAfterPressOK(texfieldController.text);
//           },
//         ),
//       ],
//     );
//   }
// }


//
// class DialogUtils {
//   static DialogUtils _instance = new DialogUtils.internal();
//
//   DialogUtils.internal();
//
//   factory DialogUtils() => _instance;
//
//   static void showCustomDialog(BuildContext context,
//       {@required String title,
//         String okBtnText = "Ok",
//         String cancelBtnText = "Cancel",
//         @required Function okBtnFunction}) {
//     showDialog(
//         context: context,
//         builder: (_) {
//           return AlertDialog(
//             title: Text(title),
//             content: /* Here add your custom widget  */,
//             actions: <Widget>[
//               FlatButton(
//                 child: Text(okBtnText),
//                 onPressed: okBtnFunction,
//               ),
//               FlatButton(
//                   child: Text(cancelBtnText),
//                   onPressed: () => Navigator.pop(context))
//             ],
//           );
//         });
//   }
// }