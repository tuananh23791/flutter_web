import 'package:flutter/material.dart';
class SectionItem extends StatefulWidget {
  final String titleSectionItem;
  final List<Widget> childs;
  var colorHeader = Colors.white;
  SectionItem({Key key, @required this.titleSectionItem, @required this.childs}) : super(key: key);
  @override
  _SectionItemState createState() => _SectionItemState();
}
class _SectionItemState extends State<SectionItem> {
  @override
  Widget build(BuildContext context) {
    return Theme(//boc va`o them de set mau2 !
      data: Theme.of(context).copyWith(accentColor: widget.colorHeader, unselectedWidgetColor: widget.colorHeader),
      child: ExpansionTile(
        title: Text(widget.titleSectionItem,
          style: TextStyle(
            color: widget.colorHeader,fontSize: 12,
            //neu ko set mau` thi default la den!
        )),
        leading: Icon(Icons.account_balance_wallet_rounded, color: widget.colorHeader,),
        backgroundColor: Colors.black,
        collapsedBackgroundColor: Colors.black,
        children: widget.childs,
      ),
    );
  }
}
