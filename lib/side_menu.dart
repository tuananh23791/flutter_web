// import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_dashboard_admin/widget/slide_menu/badge_widget.dart';

import 'constants.dart';
import 'controller.dart';
import 'widget/slide_menu/item_slide_menu.dart';
import 'widget/slide_menu/item_slide_menu_controller.dart';

class SideMenu extends StatefulWidget {
  const SideMenu({
    Key key,
  }) : super(key: key);

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance(); // init

  final ProfileController profileController = Get.find();
  var _counter = 0;

  Future<void> _incrementCounter() async {
    // final SharedPreferences prefs = await _prefs; // cho*` _prefs init xong! cach 1
    // final int counter = (prefs.getInt('counter') ?? 0) + 1; // tao bien' counter tang len 1 tu` bo nho
    // setState(() {
    //   prefs.setInt("counter", counter).then((bool success) {//write counter vao bo nho lai
    //     setState(() {
    //       _counter = counter;
    //     });
    //   });
    // });
    _prefs.then((SharedPreferences prefs) {
      // cho*` _prefs init xong! cach 2
      final int counter = (prefs.getInt('counter') ?? 0) + 1;
      prefs.setInt("counter", counter).then((bool success) {
        //write counter vao bo nho lai
        setState(() {
          _counter = counter;
        });
      });
    });
  }

  loadOldData() {
    _prefs.then((SharedPreferences prefs) {
      // cho*` _prefs init xong! cach 2
      final int counter = (prefs.getInt('counter') ?? 0);
      setState(() {
        _counter = counter;
      });
    });
  }

  //5 item total
  var isHover = [false, false, false, false];
  var isActive = [false, false, false, false];

  var section1Index = 0;
  var section2Index = 1;
  var section3Index = 2;
  var section4Index = 3;

  reset_click() {
    isActive = [false, false, false, false];
  }

  var statusLog = "";

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    loadOldData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      height: double.infinity,
      // padding: EdgeInsets.only(top: kIsWeb ? kDefaultPadding : 0),
      color: kBgLightColor,
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              //LOGO SECTION!
              Obx(() {
                return Container(
                  color: kSecondaryColor,
                  child: Row(
                    children: [
                      // : A ("assets/icons/web_hi_res_512.png")
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CircleAvatar(
                          backgroundImage: AssetImage("assets/icons/web_hi_res_512.png"),
                          radius: 30,
                        ),
                      ),
                      Text(profileController.getEmail(), style: TextStyle(color: Colors.white),),
                    ],
                  ),
                );
              }),


              // Container(
              //   height: 80,
              //   color: Colors.black12,
              //   child: Column(children: [
              //     Center(child: FlutterLogo()),
              //     Text("Counter : ${_counter}"),
              //   ]),
              // ),
              ItemSlideMenu(
                "Menu 1",
                children: [
                  subItem(text: "Sub 1", icon: Icon(Icons.account_balance_outlined, color: Colors.white,), badge_number: 100, badge_backgroundColor: Colors.green, type: 1),
                  subItem(text: "Sub 2", icon: Icon(Icons.access_alarm, color: Colors.white),badge_number: 200,badge_backgroundColor: Colors.yellow, type: 2),
                  subItem(text: "Sub 3", icon: Icon(Icons.account_balance, color: Colors.white,),badge_number: 300, badge_backgroundColor: Colors.blue, type: 3),
                  subItem(text: "Sub 4", icon: Icon(Icons.accessibility_new_outlined, color: Colors.white,),badge_number: 400, badge_backgroundColor: Colors.pink, type: 4),
                ],
                icon: Icon(Icons.account_balance_wallet_rounded,
                    color: Colors.white),
              ),
              sectionItem("PDF : davbfr", type: 4),
              sectionItem("Parsed Text", type: 5),
              sectionItem("Timer", type: 6),
              sectionItem("Listing", type: 7),
              sectionItem("Upload Image", type: 8),
              sectionItem("Form", type: 9),
              sectionItem("Google Map", type: 10),
              //VideoEditorScreen
              sectionItem("Video Editor Mobile", type: 11),
              sectionItem("Search list", type: 12),
              sectionItem("Silver custom list", type: 13),
              sectionItem("Carousel Slider", type: 14),
              sectionItem("Chart", type: 15),
            ],
          ),
        ),
      ),
    );
  }
}

Random random = new Random();
Widget sectionItem(String text, {List<ItemSlideMenu> listItem, int type}) {
  return ItemSlideMenu(
    text,
    children: listItem,
    onTap: (value) {
      Get.find<ItemSlideMenuController>().title.value = value;
      Get.find<ItemSlideMenuController>().type.value = type;
    },
  );
}

Widget subItem({String text,Widget icon,int badge_number = 0,Color badge_backgroundColor = Colors.red, int type}) {
  return ItemSlideMenu(
    text,
    icon: icon,
    badge: (badge_number >= 0) ? Badge(number: badge_number, backgroundColor: badge_backgroundColor) : SizedBox(),
    padding: EdgeInsets.only(left: 20),
    onTap: (value) {
      Get.find<ItemSlideMenuController>().title.value = value;
      Get.find<ItemSlideMenuController>().type.value = type;
    },
  );
}

//dùng ConfigurableExpansionTile khi cần cấu hình headerExpanded thay đổi !
// class Section1_configurable_expansion_tile extends StatelessWidget {
//   Widget build(BuildContext context) {
//     return ConfigurableExpansionTile(
//       header: Text("A Header Section 1"),
//       headerExpanded: Flexible(child: Text("A Header Section 1 Changed")),
//       children: [
//         Column(
//           children: [
//             Text("CHILD 1"),
//             Text("CHILD 1"),
//             Text("CHILD 1"),
//             Text("CHILD 1")
//           ],
//         ),
//         // + more params, see example !!
//       ],
//     );
//   }
// }

//Dùng ExpansionTile trong trượng hợp đơn giản nhất  default ko can quá nhiều cầu kì
class ExpansionTileWidgetPure extends StatefulWidget {
  ExpansionTileWidgetPure({Key key}) : super(key: key);

  @override
  _ExpansionTileWidgetPureState createState() =>
      _ExpansionTileWidgetPureState();
}

class _ExpansionTileWidgetPureState extends State {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text("Section 1"),
      children: [
        Column(
          children: [
            ListTile(
              mouseCursor: SystemMouseCursors.click,
              leading: Icon(Icons.accessibility),
              title: Text('Item1'),
            ),
            InkWell(onTap: (){
              print("clicked");
            },
              onHover: (isHover){
              print("is hover");
              },
            child: Text('this Text InkWell->ListTile'),

            )
          ],
        )
      ],
    );
  }
}
