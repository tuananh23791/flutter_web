import 'package:flutter/material.dart';

class DrawerNotificationItemExample {
  DrawerNotificationEnums drawerNotificationEnums;
  String value;
  Widget icon;
  Function(String) onTap;

  DrawerNotificationItemExample(
      {@required this.drawerNotificationEnums,
      @required this.value,
      @required this.onTap,
      @required this.icon});
}

enum DrawerNotificationEnums { DEFAULT, ACTIONS, MENU_OPTIONS }
