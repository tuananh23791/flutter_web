import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/screen/silver_tools/news_controller.dart';

import 'news.dart';

class NewsSection {
  final String title;
  final List<News> news;
  final NewsType newsType;
  bool isLoading = false;
  bool isCompletelyFetched = false;

  NewsSection({
    @required this.newsType,
    @required this.title,
    @required this.news,
  });
}