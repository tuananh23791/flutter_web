import 'package:flutter/material.dart';

class DropDownItemExample {
  DropDownItemEnumsExample dropDownItemEnums;
  String value;

  DropDownItemExample({@required this.dropDownItemEnums, @required this.value});
}

enum DropDownItemEnumsExample { DEFAULT, DIVIDER, TITLE }
