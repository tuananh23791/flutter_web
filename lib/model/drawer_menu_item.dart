import 'package:flutter/material.dart';

class DrawerMenuItemCustom {
  DrawerMenuItemEnums drawerMenuItemEnums;
  String value;
  Widget icon;
  Function(String) onTap;

  DrawerMenuItemCustom(
      {@required this.drawerMenuItemEnums,
      @required this.value,
      @required this.onTap,
      @required this.icon});
}

enum DrawerMenuItemEnums{
  WITH_DIVIDER,
  CLOSE,
  DEFAULT,
}