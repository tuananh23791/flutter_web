import 'package:flutter/material.dart';

class PopupMenuItemCustom {
  PopupMenuItemEnums menuItemEnums;
  String value;

  PopupMenuItemCustom({@required this.menuItemEnums, @required this.value});
}

enum PopupMenuItemEnums{
  DEFAULT,
  WITH_ICON,
}