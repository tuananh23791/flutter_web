class VendorInfo {
  int userId;
  int isPremium;
  String premiumStatus;
  String imgProfile;
  String imgVendor;
  String companyLogo;
  String clubLogo;
  String brandGuide;
  String brandSynopsis;
  String mobileCode;
  String mobile;
  String level;
  String company;
  Null vendorName;
  Null vendorDescription;
  String about;
  Null country;
  Null postalCode;
  Null unitNo;
  String add1;
  String add2;
  Null longitude;
  Null latitude;
  String statusPretty;
  int status;
  String approvedAt;

  VendorInfo(
      {this.userId,
        this.isPremium,
        this.premiumStatus,
        this.imgProfile,
        this.imgVendor,
        this.companyLogo,
        this.clubLogo,
        this.brandGuide,
        this.brandSynopsis,
        this.mobileCode,
        this.mobile,
        this.level,
        this.company,
        this.vendorName,
        this.vendorDescription,
        this.about,
        this.country,
        this.postalCode,
        this.unitNo,
        this.add1,
        this.add2,
        this.longitude,
        this.latitude,
        this.statusPretty,
        this.status,
        this.approvedAt});

  VendorInfo.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    isPremium = json['is_premium'];
    premiumStatus = json['premium_status'];
    imgProfile = json['img_profile'];
    imgVendor = json['img_vendor'];
    companyLogo = json['company_logo'];
    clubLogo = json['club_logo'];
    brandGuide = json['brand_guide'];
    brandSynopsis = json['brand_synopsis'];
    mobileCode = json['mobile_code'];
    mobile = json['mobile'];
    level = json['level'];
    company = json['company'];
    vendorName = json['vendor_name'];
    vendorDescription = json['vendor_description'];
    about = json['about'];
    country = json['country'];
    postalCode = json['postal_code'];
    unitNo = json['unit_no'];
    add1 = json['add_1'];
    add2 = json['add_2'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    statusPretty = json['status_pretty'];
    status = json['status'];
    approvedAt = json['approved_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['is_premium'] = this.isPremium;
    data['premium_status'] = this.premiumStatus;
    data['img_profile'] = this.imgProfile;
    data['img_vendor'] = this.imgVendor;
    data['company_logo'] = this.companyLogo;
    data['club_logo'] = this.clubLogo;
    data['brand_guide'] = this.brandGuide;
    data['brand_synopsis'] = this.brandSynopsis;
    data['mobile_code'] = this.mobileCode;
    data['mobile'] = this.mobile;
    data['level'] = this.level;
    data['company'] = this.company;
    data['vendor_name'] = this.vendorName;
    data['vendor_description'] = this.vendorDescription;
    data['about'] = this.about;
    data['country'] = this.country;
    data['postal_code'] = this.postalCode;
    data['unit_no'] = this.unitNo;
    data['add_1'] = this.add1;
    data['add_2'] = this.add2;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['status_pretty'] = this.statusPretty;
    data['status'] = this.status;
    data['approved_at'] = this.approvedAt;
    return data;
  }
}