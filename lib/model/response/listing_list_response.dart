import 'package:web_dashboard_admin/model/objects/listing_detail.dart';
import 'package:web_dashboard_admin/model/response/base_response.dart';

class ListingListResponse extends BaseResponse {
  bool success;
  ListingDetail primary;
  List<ListingDetail> data;

  ListingListResponse({this.success, this.primary, this.data});

  ListingListResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    primary = json['primary'] != null
        ? new ListingDetail.fromJson(json['primary'])
        : null;
    if (json['data'] != null) {
      data = <ListingDetail>[];
      json['data'].forEach((v) {
        data.add(new ListingDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.primary != null) {
      data['primary'] = this.primary.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
