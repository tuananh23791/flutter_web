import 'package:web_dashboard_admin/model/response/base_response.dart';

class ListingReplaceImageResponse extends BaseResponse {
  String link;

  ListingReplaceImageResponse({this.link});

  ListingReplaceImageResponse.fromJson(Map<String, dynamic> json) {
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['link'] = this.link;
    return data;
  }
}
