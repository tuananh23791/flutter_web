import 'package:web_dashboard_admin/model/objects/listing_detail.dart';
import 'package:web_dashboard_admin/model/response/base_response.dart';

class ListingAddResponse extends BaseResponse {
  ListingDetail data;

  ListingAddResponse({this.data});

  ListingAddResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null
        ? new ListingDetail.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}
