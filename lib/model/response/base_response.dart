class BaseResponse {
  String name;
  String message;
  int code;
  int status;
  String type;

  BaseResponse({this.name, this.message, this.code, this.status, this.type});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    message = json['message'];
    code = json['code'];
    status = json['status'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['message'] = this.message;
    data['code'] = this.code;
    data['status'] = this.status;
    data['type'] = this.type;
    return data;
  }
}
