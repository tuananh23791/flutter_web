import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart' as Dioo;

import '../network_api.dart';
import '../session_global.dart';
import '../url_app.dart';
import '../utils.dart';

class UploadFileProvider {
  Future<bool> uploadFile({String path, Uint8List byte, String fileName}) async {
    var query = {"access-token": await Session.shared.getToken()};
    print("query::::::$query");
    Dioo.FormData formData;
    if(path != null)
      formData = _getFormData(path);
    else if(byte != null)
      formData = _getFormDataWithByte(byte, fileName);

    if(formData == null) {
      print("formData null:::::::::::");
      return false;
    }
    var network = NetworkAPI(
        endpoint: url_member_upload_doc,
        jsonQuery: query,
        formData: formData);
//    Get.defaultDialog(title:"",content:CircularPercent());
    var jsonRespondBody = await network.callAPI(
        showLog: true, method: "POST", percent: (percent) {
          print("percent::::::$percent");
//          if(percent == 1){
//            Get.back();
//          }
    });

    if (jsonRespondBody["code"] == 100) {
      print("success uploadFile");
      return true;
    } else {
      print("error uploadFile");
      return false;
    }
  }

  Dioo.FormData _getFormData(String path) {
    String fileName = path.split('/').last;
    var multipartFile =
    Dioo.MultipartFile.fromFile(path, filename: fileName);
    var formData = new Dioo.FormData.fromMap({
      "field": "img_memorandum",
      "file": multipartFile,
    });

    return formData;
  }

  Dioo.FormData _getFormDataWithByte(Uint8List byte, String fileName) {
    var multipartFile =
    Dioo.MultipartFile.fromBytes(byte, filename: fileName);
    var formData = new Dioo.FormData.fromMap({
      "field": "img_memorandum",
      "file": multipartFile,
    });

    return formData;
  }
}
