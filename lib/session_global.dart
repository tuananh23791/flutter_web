import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive/hive.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_dashboard_admin/config_setting.dart';
import 'package:web_dashboard_admin/controller.dart';
import 'package:web_dashboard_admin/screen/after_login.dart';

import 'alertbox_custom.dart';
//https://pub.dev/packages/flutter_easyloading
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'main_screen.dart';
import 'screen/login_screen.dart';
import 'utils.dart';

class Session {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  static final Session _singleton = Session._internal();
  factory Session() {
    return _singleton;
  }
  Session._internal();
  static Session get shared => _singleton;
  var baseURL = ConfigSetting.baseURLFirstload;
  Color randomColor() {
    return Color(Random().nextInt(0xffffffff));
    // return Colors.teal[100 * (Random().nextInt() % 9.0)];
  }

  showLoading(){
    EasyLoading.instance.userInteractions = false;
    EasyLoading.show(status: 'Loading...');
    // EasyLoading.showProgress(0.5, status: 'Loading...');// cho 0.5 ko xong moi show len
  }
  hideLoading(){
    EasyLoading.dismiss();
  }

  getBaseURL() {
    if (baseURL.endsWith('/')) {
      return baseURL;
    } else {
      return baseURL + '/';
    }
  }


  showAlert({@required BuildContext context, String title, String msg, Function callback}){
    showDialog(context: context, builder: (_) => AlertPopupOneButton(title: title, content: msg,callbackAfterPressOK: (){
      Navigator.of(context).pop();
      callback();
    },));
  }

  showAlertPopup2ButtonWithCallback(
      {
        BuildContext context,
        String title = '',
        String msg = '',
        Function callback,
        String titleButtonRight = "OK",
        String titleButtonLeft = "Cancle"}) {
    showDialog(context: context, builder: (_) => AlertPopup2Button(
      titleButtonRight: titleButtonRight,
      titleButtonLeft: titleButtonLeft,
      title: title, content: msg,callbackAfterPressOK: callback,));
  }

  saveIntData() {

  }

  saveToken(String newToken) async {
    var box = await Hive.openBox('myBox');
    // var box = Hive.box('myBox');
    box.put('token', newToken);
    var token = box.get('token');
    print('token: $token');
  }

  getToken() async {
    var box = await Hive.openBox('myBox');
    // var box = Hive.box('myBox');
    var token = box.get('token');
    print('token: $token');
    return token;
  }

  removeToken() async {
    var box = await Hive.openBox('myBox');
    // var box = Hive.box('myBox');
    box.delete('token');
    var token = box.get('token');
    print('token: ${token == null ? "nulllllll" : token}');
  }
  Future<void> logout(BuildContext context) async {
    // controller handle
    Session.shared.showLoading();
    await Session.shared.removeToken();
    Session.shared.hideLoading();
    final ProfileController profileController = Get.find();
    profileController.onDelete();
    Utils.shared.changeScreenAndRemove(context, LoginScreen());
  }

  showPickerDate(BuildContext context, Function(DateTime) afterPick) {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1900, 1, 1),
        maxTime: DateTime.now(),
        // maxTime: DateTime(Session.shared.getMinAllowYear(), 31, 12),
        // theme: DatePickerTheme(
        //     // headerColor: Colors.orange,
        //     // backgroundColor: Colors.blue,
        //     // itemStyle: TextStyle(
        //     //     color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
        //     // doneStyle: TextStyle(color: Colors.white, fontSize: 16)
        // ),
        onChanged: (date) {
          print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
          // controller.handleAfterPickerDate(date);
        }, onConfirm: (date) {
          print('confirm $date');
          afterPick(date);
          // controller.handleAfterPickerDate(date);
        }, currentTime: DateTime.now(), locale: LocaleType.en);
  }

  goAfterLogin(BuildContext context){
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(
      fullscreenDialog: true,
      builder: (context) => AfterLoginScreen(),
    ));
  }
  goDashBoard(BuildContext context){
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(
      fullscreenDialog: true,
      builder: (context) => MainScreen(),
    ));
  }

}
