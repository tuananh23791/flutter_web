import 'package:flutter/material.dart';

// All of our constant stuff

const kPrimaryColor = Color.fromARGB(255, 27, 35, 48);//Color(0xFF366CF6);
const kSecondaryColor = Color.fromARGB(255, 37, 47, 62);//Color(0xFFF5F6FC);
const kBgLightColor = Color(0xFFE7E7E7);
const kBgDarkColor = Color(0xFFEBEDFA);
const kBadgeColor = Color(0xFFEE376E);
const kGrayColor = Color(0xFF8793B2);
const kTitleTextColor = Color(0xFF30384D);
const kTextColor = Color(0xFF4D5875);

const kDefaultPadding = 20.0;
const placeHolderImage = 'assets/images/loading3.gif';