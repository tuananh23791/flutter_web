import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

import 'ModelErrorJson.dart';
import 'config_setting.dart';
import 'session_global.dart';

var logger = Logger(
  printer: PrettyPrinter(
    methodCount: 0,
    printTime: true,
  ),
);
class NetworkAPI {
  var endpoint = "";
  Map<String, dynamic> jsonQuery;
  FormData formData;
  NetworkAPI({
    this.endpoint,
    this.jsonQuery,
    this.formData,
  });
  var dio = Dio(
    BaseOptions(
        baseUrl: Session.shared.getBaseURL(),
        connectTimeout: 5000,
        receiveTimeout: 3000),
  );
  Future callAPIGetSetting() async {
    print("callAPIGetSetting");
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var username = ConfigSetting.username;
    var password = ConfigSetting.password;
    var basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));
    print(basicAuth);
    Response response;
    dio.options.headers = {'authorization': basicAuth};
    print("callAPIGetSetting body jsonQuery: $jsonQuery");
    // print("FullURL: ${Session.shared.getBaseURL()}$endpoint");
    var fullUrl = '${Session.shared.getBaseURL()}$endpoint';
    print("FullURL: $fullUrl");
    try {

      print("start get response ==>>");
      response = await dio.get(endpoint, queryParameters: jsonQuery);
      // response = await dio.get(endpoint + "aa", queryParameters: jsonQuery);//404 test error url
      // print(response.data);
      print("done response ==>>");
      print(response);
      if (response.statusCode == 200) {
        // print("code 200 OK");
        logger.d(response.data);
        return response.data;
      } else {
        // return handleAPIError(response);
        print("GET $fullUrl response.data ❌ => ${response.data}");
        var modelErrorJson = ModelErrorJson.fromJson(response.data);
        print("GET $fullUrl ❌ => ${modelErrorJson.toJson()}");
        logger.d(modelErrorJson.toJson());
        return modelErrorJson;
      }
    } on DioError catch (e) {
      print("$fullUrl ❌❌ => ${e}");//for showing only here
      return e;
    }
  }
  Future callAPI({method = "POST", showLog = false, keepKeyboard = false, Function(double) percent}) async {
    if (keepKeyboard == false) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    }

    Response response;
    var fullUrl = '${Session.shared.getBaseURL()}$endpoint';
    print("FullURL: $fullUrl");
    try {
      if (method == "POST") {
        response =
        await dio.post(endpoint, queryParameters: jsonQuery, data: formData,
          onSendProgress: (int sent, int total) {
            if (percent != null) {
              var perCent = sent.toDouble()/total.toDouble();
              percent(perCent);
            }
            // print('$sent/$total');
            // print('$sent/$total');
            // var perCent = sent.toDouble()/total.toDouble();
            // percent(perCent);
          },
        );
      } else {
        response = await dio.get(endpoint, queryParameters: jsonQuery);
      }

      if (showLog) {
        // logger.d(response.request.data.toString());
        logger.d(jsonQuery);
        logger.d(response.data);
      }
      // return response.data;
      if (response.statusCode == 200) {
        print("$fullUrl ✅");
        return response.data;
      } else {
        // return handleAPIError(response);
        var modelErrorJson = ModelErrorJson.fromJson(response.data);
        print("POST $fullUrl ❌ => ${modelErrorJson.getMessage()}");
        if (showLog) {
          //jsonQuery
          logger.d(modelErrorJson.toJson());
        }
        return response.data;
        // return modelErrorJson;
      }
    } on DioError catch (e) {
      //map error data to json
      //
      // if (e.response.statusCode == 404) {
      //   Session.shared.showAlertPopupOneButtonNoCallBack(
      //       content: "$fullUrl ${e.response.statusCode } Not found");
      //
      // } else {
      //   // var model = ModelErrorJson.fromJson(e.response.data);
      //   print("$method $fullUrl ❌❌ => ${e.response.data}");//for showing only here
      //   return e.response.data;
      //
      // }
      print("$method $fullUrl ❌❌ => ${e.response.data}");//for showing only here
      return e.response.data;
    }
  }


}
