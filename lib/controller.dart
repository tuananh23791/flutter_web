import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:universal_html/js.dart';

import 'model/ModelUserProfile.dart';
import 'network_api.dart';
import 'session_global.dart';
import 'url_app.dart';
class ProfileController extends GetxController {

  final box = GetStorage();
  bool get isDark => box.read('darkmode') ?? false;
  ThemeData get theme => isDark ? ThemeData.dark() : ThemeData.light();
  void changeTheme(bool val) => box.write('darkmode', val);

  var userProfile = ModelUserProfile().obs;
  getProfileImage(){
    //for dynamic should check here
    if (getIsCompany()) {
      return userProfile.value.company_logo ?? "";
    } else {
      return userProfile.value.imgProfile ?? "";
    }


  }

  getFullname(){
    //for dynamic should check here
    return userProfile.value.fullname ?? "null";
  }
  getEmail(){
    //for dynamic should check here
    return userProfile.value.email ?? "";

  }

  bool getIsCompany() {
    return (userProfile.value.isVendor == '1');
  }
  callAPIGetProfile(BuildContext context) async {
    print("start callAPIGetProfile");
    var query = {"access-token": await Session.shared.getToken()};
    var network = NetworkAPI(
        endpoint: url_member_info,
        jsonQuery: query);
    print("query $query");
    var jsonRespondBody = await network.callAPI(showLog: true, method: "GET");

    if (jsonRespondBody["code"] == 100) {
      userProfile.value = ModelProfileResult.fromJson(jsonRespondBody).data;
      print("success callAPIGetProfile");

    } else {
      print("error callAPIGetProfile");

      if (jsonRespondBody["code"] == 0) {
        Session.shared.showAlert(title: "Session Expire", msg: "Please try login again",callback: (){
          Session.shared.logout(context);

        });
      } else {
        Session.shared.showAlert(msg: jsonRespondBody["message"] ?? "");
      }

    }
  }
}



class ModelProfileResult {
  int code;
  ModelUserProfile data;
  ModelProfileResult({this.code, this.data});
  ModelProfileResult.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    data = json['data'] != null ? new ModelUserProfile.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}
