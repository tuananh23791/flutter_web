import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/controller.dart';
// import 'package:get_storage/get_storage.dart';
import 'package:web_dashboard_admin/screen/login_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'auth_dialog.dart';
import 'helper/firebase_messaging.dart';
import 'main_screen.dart';


void main() {
  runApp(MyApp());
  FBMessaging.instance.init();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        backgroundColor: Colors.white,
        cardColor: Colors.blueGrey[50],
        primaryTextTheme: TextTheme(
          button: TextStyle(
            color: Colors.blueGrey,
            decorationColor: Colors.blueGrey[300],
          ),
          subtitle2: TextStyle(
            color: Colors.blueGrey[900],
          ),
          subtitle1: TextStyle(
            color: Colors.black,
          ),
          headline1: TextStyle(color: Colors.blueGrey[800]),
        ),
        bottomAppBarColor: Colors.blueGrey[900],
        iconTheme: IconThemeData(color: Colors.blueGrey),
      ),
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),//LoginScreen  AuthDialog MainScreen NewsPage
      builder: EasyLoading.init(),
      // home: LoginScreen()
    );
  }
}
///Upgrade 2. for Web
///downgrade 1.26 for carkee
///1. check flutter --version
///Flutter 1.22.6 • channel unknown • unknown source, this branch is my branch for local stable_1.22.6
///Change stable 2. for current by command flutter channel
///Downloading Dart SDK from Flutter engine 40441def692f444660a11e20fac37af9050245ab...
