import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/screen/chart/line_chart.dart';

import 'bar_chart.dart';
import 'circle_chart.dart';

class ChartScreen extends StatelessWidget {
  const ChartScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(child: Text("Line"),),
                Tab(child: Text("Bar"),),
                Tab(child: Text("Circle"),),
              ],
            ),
            title: Text(''),
          ),
          body: TabBarView(
            children: [
              LineChartSample(),
              BarChartSample(),
              CircleChart(),
            ],
          ),
        ),
      ),
    );
  }
}
