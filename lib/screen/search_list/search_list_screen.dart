import 'package:flutter/material.dart';

class SearchListScreen extends StatefulWidget {
  @override
  _SearchListScreenState createState() => _SearchListScreenState();
}

class _SearchListScreenState extends State<SearchListScreen> {
  List<String> _listString = [
    "1a",
    "2b",
    "3c",
    "4d",
    "5e",
    "6a",
    "7b",
    "8c",
    "9d",
    "10e",
    "11ab",
    "12bc",
    "13cd",
    "14de",
    "15ef",
  ];
  List<String> _listSearch = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listSearch.addAll(_listString);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [searchField(), _listViewItem()],
      ),
    );
  }

  Widget searchField() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'Please enter search name',
        hintStyle: TextStyle(
          fontSize: 18,
          color: Colors.grey,
          fontWeight: FontWeight.bold,
          fontFamily: 'HelveticaNeue',
        ),
        contentPadding: EdgeInsets.only(left: 15)
      ),
      onChanged: (data) {
        _searchData(data);
      },
    );
  }

  Widget _listViewItem() {
    return Expanded(
        child: ListView.builder(
      itemBuilder: (context, i) => item(_listSearch[i]),
      itemCount: _listSearch.length,
    ));
  }

  Widget item(String data) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 1.0))),
      height: 50,
      child: Align(alignment: Alignment.centerLeft, child: Text(data)),
      padding: EdgeInsets.only(left: 15),
    );
  }

  _searchData(String data) {
    _listSearch.clear();
    _listString.forEach((value) {
      if (value.contains(data)) {
        _listSearch.add(value);
      }
    });

    setState(() {});
  }
}
