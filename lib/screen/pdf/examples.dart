import 'dart:async';
import 'dart:typed_data';

import 'package:pdf/pdf.dart';
import 'package:web_dashboard_admin/screen/pdf/data.dart';
import 'package:web_dashboard_admin/screen/pdf/examples_form/calendar.dart';
import 'package:web_dashboard_admin/screen/pdf/examples_form/certificate.dart';
import 'package:web_dashboard_admin/screen/pdf/examples_form/document.dart';
import 'package:web_dashboard_admin/screen/pdf/examples_form/invoice.dart';
import 'package:web_dashboard_admin/screen/pdf/examples_form/report.dart';
import 'package:web_dashboard_admin/screen/pdf/examples_form/resume.dart';


const examples = <Example>[
  Example('CALENDAR', 'calendar.dart', generateCalendar),
  Example('DOCUMENT', 'document.dart', generateDocument),
  Example('RÉSUMÉ', 'resume.dart', generateResume),
  Example('INVOICE', 'invoice.dart', generateInvoice),
  Example('REPORT', 'report.dart', generateReport),
  Example('CERTIFICATE', 'certificate.dart', generateCertificate, true),
];

typedef LayoutCallbackWithData = Future<Uint8List> Function(
    PdfPageFormat pageFormat, CustomData data);

class Example {
  const Example(this.name, this.file, this.builder, [this.needsData = false]);

  final String name;

  final String file;

  final LayoutCallbackWithData builder;

  final bool needsData;
}
