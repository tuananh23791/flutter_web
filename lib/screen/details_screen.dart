import 'package:flutter/material.dart';
class DetailsScreen extends StatelessWidget {

  final int param_number;

  const DetailsScreen({Key key, this.param_number}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('DetailsScreen $param_number'),
        ),
        body: bodyWidget(context));
  }
  bodyWidget(BuildContext context) {
    return Container(color: Colors.yellow);
  }
}
