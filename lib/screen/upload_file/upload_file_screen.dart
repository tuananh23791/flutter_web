import 'dart:html' as html;
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:web_dashboard_admin/controller.dart';
import 'package:web_dashboard_admin/screen/upload_file/upload_file_controller.dart';

class UploadFileScreen extends StatefulWidget {
  @override
  _UploadFileState createState() => _UploadFileState();
}

class _UploadFileState extends State<UploadFileScreen> {
  final _picker = ImagePicker();

  final ProfileController profileController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox.expand(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() {
              //0 is image - 1 is pdf
              if (profileController.userProfile.value.imgMemorandumMimeType != 'application/pdf') {
                return profileController.userProfile.value.imgMemorandum.isEmpty ? SizedBox() : _image();
              } else {
//                return PdfView(url: Get.find<UploadFileController>().imgMemorandumUrl.value,width: 500,height: 400,);
                return InkWell(
                  child: Text(
                    "View ${Get.find<UploadFileController>().fileName}",
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onTap: () {
                    html.window.open(
                        profileController.userProfile.value.imgMemorandum,
                        "pdf");
                  },
                );
              }
            }),
            SizedBox(
              height: 30,
            ),
            _button("Upload", width: 100, height: 50, onPressed: _showPopup),
          ],
        ),
      ),
    );
  }

  _pickImage() async {
    final pickedFile = await _picker.getImage();
    if (kIsWeb) {
      Uint8List byte = await pickedFile.readAsBytes();
      Get.back();
//      Navigator.of(context).pop();
      Get.find<UploadFileController>()
          .uploadFile(context, byte: byte, fileName: "1", type: 0);
    } else {
      //do something for other platform
    }
  }

  _pickPdfFile() async {
    print("_pickPdfFile::::");
    FilePickerResult result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );
    if (result != null) {
      PlatformFile file = result.files.first;
      if (kIsWeb) {
        Get.back(closeOverlays: true);
        Get.find<UploadFileController>().uploadFile(context,
            byte: file.bytes, fileName: file.name, type: 1);
      } else {
        //do something for other platform
      }
    } else {
      // User canceled the picker
    }
  }

  final Color _defaultColor = Color.fromARGB(255, 27, 35, 48);

  _showPopup() {
    Get.defaultDialog(title: "Select Option", content: _popupUploadFile());
  }

  Widget _popupUploadFile() {
    return Container(
      child: Column(
        children: [
          _button("Upload Image",
              width: 200, height: 50, onPressed: _pickImage),
          SizedBox(
            height: 30,
          ),
          _button("Upload Pdf", width: 200, height: 50, onPressed: _pickPdfFile)
        ],
      ),
    );
  }

  Widget _button(String text,
      {double width, double height, Function onPressed}) {
    return Container(
      width: width ?? 100,
      height: height ?? 50,
      child: TextButton(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        ),
        onPressed: onPressed ?? () {},
        child: Text(text ?? "Button"),
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: _defaultColor),
    );
  }

  Widget _image(){
    return Container(
      width: 300,
      height: 300,
      child: Center(
        child: Image.network(
          profileController.userProfile.value.imgMemorandum,
          fit: BoxFit.scaleDown,
          loadingBuilder: (BuildContext context, Widget child,
              ImageChunkEvent loadingProgress) {
            if (loadingProgress == null) return child;
            return Center(
              child: CircularProgressIndicator(
                value: loadingProgress.expectedTotalBytes !=
                    null
                    ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes
                    : null,
              ),
            );
          },
        ),
      ),
    );
  }
}
