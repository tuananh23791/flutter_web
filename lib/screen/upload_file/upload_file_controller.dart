import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/model/ModelUserProfile.dart';
import 'package:web_dashboard_admin/services/upload_provider.dart';

import '../../controller.dart';
import '../../network_api.dart';
import '../../session_global.dart';
import '../../url_app.dart';

class UploadFileController extends GetxController{
  var imgMemorandumUrl = "".obs;
  var type = 0.obs; //0 is image - 1 is pdf
  String fileName = "";

  uploadFile(BuildContext context, {Uint8List byte, String fileName, int type}) async {
    var isUploadSuccess = false;
    this.fileName = fileName;
    Get.dialog(Center(child: CircularProgressIndicator()),
        barrierDismissible: false);
    isUploadSuccess = await UploadFileProvider().uploadFile(byte: byte, fileName: fileName);
    Get.back();

    if(isUploadSuccess){
      final ProfileController profileController = Get.find();
      await profileController.callAPIGetProfile(context);
    }
  }

  // callAPIGetProfile(BuildContext context, {int type}) async {
  //   print("start callAPIGetProfile");
  //   var query = {"access-token": await Session.shared.getToken()};
  //   var network = NetworkAPI(
  //       endpoint: url_member_info,
  //       jsonQuery: query);
  //   print("query $query");
  //   var jsonRespondBody = await network.callAPI(showLog: true, method: "GET");
  //
  //   if (jsonRespondBody["code"] == 100) {
  //     ModelUserProfile userProfile = ModelProfileResult.fromJson(jsonRespondBody).data;
  //     this.type.value = type;
  //     imgMemorandumUrl.value = userProfile.imgMemorandum;
  //     print("url:::::::${userProfile.imgMemorandum}");
  //     print("success callAPIGetProfile");
  //
  //   } else {
  //     print("error callAPIGetProfile");
  //
  //     if (jsonRespondBody["code"] == 0) {
  //       Session.shared.showAlert(title: "Session Expire", msg: "Please try login again",callback: (){
  //         Session.shared.logout(context);
  //       });
  //     } else {
  //       Session.shared.showAlert(msg: jsonRespondBody["message"] ?? "");
  //     }
  //
  //   }
  // }
}