import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:web_dashboard_admin/screen/form/user.dart';
import 'package:web_dashboard_admin/session_global.dart';
import 'package:web_dashboard_admin/widget/dialogs/custom_dialog.dart';

import 'picker_dropdown_field.dart';

class FormScreen extends StatefulWidget {
  @override
  _FormScreenState createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  final _formKey = GlobalKey<FormState>();
  final _user = User();
  String _value;
  List<String> _listString = ["1a", "2b", "3c", "4d", "5e", "6a", "7b", "8c", "9d", "10e"];
  List<String> _listSearch = [];

  var dropdownControler = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _value = _listString[0];
  }

  handleAfterPickerDate(DateTime chooseDate) {
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(chooseDate);
    dropdownControler.text = formattedDate;
    // registration_code = formattedDate;
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 2,
        margin: EdgeInsets.fromLTRB(64, 32, 64, 64),
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Builder(
                  builder: (context) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      listCountry(),
                      Container(
                        width: 150,
                          height: 50,
                          child: dropdownButton()),
                      autocompleteTextField(),
                      Form(
                          key: _formKey,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                PickerDropDown(//use this for any field need ropdown ui
                                  controller: dropdownControler,
                                  onTap: (){
                                    CustomDialog().showAlertDialog(title: "Alert", message: "You clicked PickerDropDown");
                                  },
                                ),
                                PickerDropDown(//use this for any field need ropdown ui
                                  controller: dropdownControler,
                                  onTap: (){
                                    Session.shared.showPickerDate(context, (date) {
                                      handleAfterPickerDate(date);
                                    });
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(
                                    fontFamily: 'HelveticaNeue',
                                  ),
                                  decoration: InputDecoration(
                                    labelText: 'First name',
                                    labelStyle: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'HelveticaNeue',
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter your first name';
                                    }

                                    return null;
                                  },
                                  onSaved: (val) =>
                                      setState(() => _user.firstName = val),
                                ),
                                TextFormField(
                                    decoration: InputDecoration(
                                      labelText: 'Last name',
                                      labelStyle: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'HelveticaNeue',
                                      ),
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter your last name.';
                                      }

                                      return null;
                                    },
                                    onSaved: (val) =>
                                        setState(() => _user.lastName = val)),
                                Container(
                                  padding: const EdgeInsets.fromLTRB(0, 50, 0, 20),
                                  child: Text(
                                    'Subscribe',
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'HelveticaNeue',
                                    ),
                                  ),
                                ),
                                SwitchListTile(
                                    title: const Text(
                                      'To our Monthly Subscription',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontFamily: 'HelveticaNeue',
                                      ),
                                    ),
                                    value: _user.monthlyNewsletter,
                                    onChanged: (bool val) => setState(
                                        () => _user.monthlyNewsletter = val)),
                                SwitchListTile(
                                    title: const Text(
                                      'To our Yearly Subscription',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontFamily: 'HelveticaNeue',
                                      ),
                                    ),
                                    value: _user.yearlyNewsletter,
                                    onChanged: (bool val) => setState(
                                        () => _user.yearlyNewsletter = val)),
                                SwitchListTile(
                                    title: const Text(
                                      'To our Weekly Subscription',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontFamily: 'HelveticaNeue',
                                      ),
                                    ),
                                    value: _user.weeklyNewsletter,
                                    onChanged: (bool val) => setState(
                                        () => _user.weeklyNewsletter = val)),
                                Container(
                                  padding: const EdgeInsets.fromLTRB(0, 50, 0, 20),
                                  child: Text(
                                    'Interests',
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'HelveticaNeue',
                                    ),
                                  ),
                                ),
                                checkbox("Writing",
                                    onChanged: (val) => setState(() =>
                                        _user.passions[User.PassionWriting] = val),
                                    value: _user.passions[User.PassionWriting]),
                                checkbox("Travelling",
                                    onChanged: (val) => setState(() =>
                                        _user.passions[User.PassionSinging] = val),
                                    value: _user.passions[User.PassionSinging]),
                                checkbox("Travelling",
                                    onChanged: (val) => setState(() => _user
                                        .passions[User.PassionTraveling] = val),
                                    value: _user.passions[User.PassionTraveling]),
                                checkbox("Cooking",
                                    onChanged: (val) => setState(() =>
                                        _user.passions[User.PassionCooking] = val),
                                    value: _user.passions[User.PassionCooking]),
                                Container(
                                    height: 80,
                                    // margin: EdgeInsets.only(left: 200, right: 200),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 16.0, horizontal: 16.0),
                                    child: RaisedButton(
                                        color: Colors.blue,
                                        onPressed: () {
                                          if (_formKey.currentState.validate()) {
                                            _formKey.currentState.save();
                                            _user.save();
                                            _showDialog();
                                            print(_user);
                                            print(_user.firstName);
                                          }
                                        },
                                        child: Text(
                                          'Save',
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'HelveticaNeue',
                                          ),
                                        ))),
                              ])),
                    ],
                  ))),
        ),
      ),
    );
  }

  Widget checkbox(String text, {Function(bool) onChanged, bool value}) {
    return CheckboxListTile(
        title: Text(
          text ?? "Cooking",
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
            fontFamily: 'HelveticaNeue',
          ),
        ),
        value: value,
        onChanged: (val) {
          onChanged(val);
        });
  }

  Widget listCountry() {
    return CountryCodePicker(
      onChanged: print,
      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
      initialSelection: 'IT',
//      favorite: ['+39','FR'],
      // optional. Shows only country name and flag
      showCountryOnly: true,
      // optional. Shows only country name and flag when popup is closed.
      showOnlyCountryWhenClosed: true,
      // optional. aligns the flag and the Text left
      alignLeft: false,
    );
  }

  Widget dropdownButton() {
    return DropdownButton<String>(
      isExpanded: true,
        value: _value,
        onChanged: (value) {
          setState(() {
            _value = value;
          });
        },
        items: _listString.map((value) => DropdownMenuItem(
              child: Text(
                value,
                style: new TextStyle(color: Colors.black),
              ),
              value: value,
            )).toList());
  }

  Widget autocompleteTextField(){
    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
          autofocus: false,
          style: DefaultTextStyle.of(context).style.copyWith(
              fontStyle: FontStyle.italic
          ),
          decoration: InputDecoration(
              border: OutlineInputBorder()
          )
      ),
      suggestionsCallback: (pattern) async {
        _listSearch.clear();
        _listString.forEach((value) {
          if(value.contains(pattern)){
            _listSearch.add(value);
          }
        });
        return _listSearch;
      },
      itemBuilder: (context, data) {
        return ListTile(
          leading: Icon(Icons.shopping_cart),
          title: Text("title: $data"),
          subtitle: Text("subtitle: $data"),
        );
      },
      onSuggestionSelected: (data) {
        _showDialog(content: data);
      },

    );
  }

  _showDialog({String content}) {
    Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text(content??'Submitting form')));
  }
}
