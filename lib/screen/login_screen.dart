import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/auth_dialog.dart';
import 'package:web_dashboard_admin/session_global.dart';

import '../ModelErrorJson.dart';
import '../config_setting.dart';
import '../controller.dart';
import '../network_api.dart';
import '../url_app.dart';
import 'ModelEndpoint.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final ProfileController profileController = Get.put(ProfileController(),permanent: true);
  callAPIGetEndpoint() async {
    Session.shared.showLoading();
    print("start callAPIGetEndpoint");
    var network = NetworkAPI(endpoint: url_getsetting, jsonQuery: {
      "v": ConfigSetting.appVersion,
      "e": "ios",
      "c": ConfigSetting.clubName
    });
    var jsonBody = await network.callAPIGetSetting();
    Session.shared.hideLoading();
    print("done call api");

    if (jsonBody["code"] == 100) {
      var modelEndpoint = ModelEndpoint.fromJson(jsonBody);
      Session.shared.baseURL = modelEndpoint.endpoint;

    } else {
      print("❌❌❌❌ ${jsonBody["message"]}");
      Session.shared.showAlert(
          context: context,
          title: "Error",
          msg: jsonBody["message"] ?? "",
          callback: () {
            print("clicked after error");
          });
    }
  }

  @override
  Widget build(BuildContext context) => FocusDetector(
        onFocusLost: () {
          logger.i(
            'Focus Lost.'
            '\nTriggered when either [onVisibilityLost] or [onForegroundLost] '
            'is called.'
            '\nEquivalent to onPause() on Android or viewDidDisappear() on iOS.',
          );
        },
        onFocusGained: () {
          logger.i(
            'Focus Gained.'
            '\nTriggered when either [onVisibilityGained] or [onForegroundGained] '
            'is called.'
            '\nEquivalent to onResume() on Android or viewDidAppear() on iOS.',
          );
          callAPIGetEndpoint();
        },
        onVisibilityLost: () {
          logger.i(
            'Visibility Lost.'
            '\nIt means the widget is no longer visible within your app.',
          );
        },
        onVisibilityGained: () {
          logger.i(
            'Visibility Gained.'
            '\nIt means the widget is now visible within your app.',
          );
        },
        onForegroundLost: () {
          logger.i(
            'Foreground Lost.'
            '\nIt means, for example, that the user sent your app to the '
            'background by opening another app or turned off the device\'s '
            'screen while your widget was visible.',
          );
        },
        onForegroundGained: () {
          logger.i(
            'Foreground Gained.'
            '\nIt means, for example, that the user switched back to your app '
            'or turned the device\'s screen back on while your widget was visible.',
          );
        },
        child: AuthDialog(),
      );
}
