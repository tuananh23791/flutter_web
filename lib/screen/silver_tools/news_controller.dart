import 'package:get/get.dart';
import 'package:web_dashboard_admin/data/news_data.dart';
import 'package:web_dashboard_admin/model/news.dart';
import 'package:web_dashboard_admin/model/news_section.dart';

enum NewsType { top, latest }

class NewsController extends GetxController {
  final _newsSections = [
    NewsSection(title: 'Top News', newsType: NewsType.top, news: []),
    NewsSection(title: 'Latest News', newsType: NewsType.latest, news: []),
  ];

  NewsController() {
    fetchInitialStories(NewsType.latest);
    fetchInitialStories(NewsType.top);
  }

  List<NewsSection> get newsSections => _newsSections;

  List<News> get _topStories => _newsSections
      .where((element) => element.newsType == NewsType.top)
      .first
      .news;

  List<News> get _latestStories => _newsSections
      .where((element) => element.newsType == NewsType.latest)
      .first
      .news;

  Future fetchInitialStories(NewsType type) async {
    final news1 =
        _newsSections.where((element) => element.newsType == type).first;
    news1.isLoading = true;
    update();

    await Future.delayed(Duration(seconds: 1));
    switch (type) {
      case NewsType.top:
        {
          _topStories.addAll(topStories.sublist(0, 5));
          update();
          break;
        }
      case NewsType.latest:
        {
          _latestStories.addAll(newStories.sublist(0, 5));
          update();
          break;
        }
      default:
        break;
    }

    final news2 =
        _newsSections.where((element) => element.newsType == type).first;
    news2.isLoading = false;
    update();
  }

  Future fetchMoreStories(NewsType type) async {
    final news1 =
        _newsSections.where((element) => element.newsType == type).first;
    news1.isLoading = true;
    update();

    await Future.delayed(Duration(milliseconds: 600));
    switch (type) {
      case NewsType.top:
        {
          _topStories.addAll(topStories.sublist(5));
          update();
          break;
        }
      case NewsType.latest:
        {
          _latestStories.addAll(newStories.sublist(5));
          update();
          break;
        }
      default:
        break;
    }

    final news2 =
        _newsSections.where((element) => element.newsType == type).first;
    news2.isCompletelyFetched = true;
    news2.isLoading = false;
    update();
  }
}
