import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:web_dashboard_admin/model/news.dart';
import 'package:web_dashboard_admin/model/news_section.dart';
import 'package:web_dashboard_admin/screen/silver_tools/news_controller.dart';
import 'package:web_dashboard_admin/screen/silver_tools/widget/card_background_widget.dart';
import 'package:web_dashboard_admin/screen/silver_tools/widget/card_header_widget.dart';
import 'package:web_dashboard_admin/screen/silver_tools/widget/load_more_button_widget.dart';
import 'package:web_dashboard_admin/screen/silver_tools/widget/news_tile_widget.dart';

class NewsSectionWidget extends StatelessWidget {
  final NewsSection section;
  final List<News> news;
  final Function(int) onBookMark;

  const NewsSectionWidget({
    Key key,
    @required this.section,
    @required this.news,@required this.onBookMark,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SliverPadding(
        padding: EdgeInsets.all(CardHeaderWidget.padding).copyWith(top: 0),
        sliver: MultiSliver(
          pushPinnedChildren: true,
          children: [
            SliverStack(
              insetOnOverlap: true,
              children: [
                SliverPositioned.fill(
                  top: CardHeaderWidget.padding,
                  child: CardBackgroundWidget(),
                ),
                buildCard(context),
              ],
            ),
          ],
        ),
      );

  Widget buildCard(BuildContext context) => MultiSliver(
        children: <Widget>[
          SliverPinnedHeader(child: CardHeaderWidget(title: section.title)),
          SliverClip(
            child: MultiSliver(
              children: <Widget>[
                buildNews(),
                SliverToBoxAdapter(child: buildLoadMore(context)),
              ],
            ),
          ),
        ],
      );

  Widget buildNews() => SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) => NewsTileWidget(
            news: news[index],
            onSave: () {
              onBookMark(index);
            },
          ),
          childCount: news.length,
        ),
      );

  Widget buildLoadMore(BuildContext context) {
    if (section.isCompletelyFetched) return Container();

    return LoadMoreButtonWidget(
      loading: section.isLoading,
      nextPage: () {
        Get.find<NewsController>().fetchMoreStories(section.newsType);
      },
    );
  }
}
