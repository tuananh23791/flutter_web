import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/model/news.dart';

class NewsTileWidget extends StatelessWidget {
  final News news;
  final Function onSave;

  const NewsTileWidget({
    Key key,
    @required this.news,
    @required this.onSave,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 150,
            width: double.infinity,
            child: Image.network(news.imageUrl, fit: BoxFit.cover),
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            title: Text(news.title),
            trailing: IconButton(
              icon: Icon(
                Icons.favorite,
                color: news.isSaved ? Colors.deepOrange : Colors.grey,
              ),
              onPressed: onSave,
            ),
          ),
          Divider(height: 1),
        ],
      );
}
