import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/model/news_section.dart';
import 'package:web_dashboard_admin/screen/silver_tools/news_controller.dart';
import 'package:web_dashboard_admin/screen/silver_tools/widget/news_section_widget.dart';

class CustomSilversPage extends StatefulWidget {
  @override
  _CustomSilversPageState createState() => _CustomSilversPageState();
}

class _CustomSilversPageState extends State<CustomSilversPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NewsController>(
      builder: (news) => CustomScrollView(
        slivers: buildNews(news: news.newsSections),
      ),
    );
  }

  List<Widget> buildNews({@required List<NewsSection> news}) => news
      .map((newsSection) => NewsSectionWidget(
            section: newsSection,
            news: newsSection.news,
            onBookMark: (index) {
              setState(() {
                newsSection.news[index].isSaved =
                !newsSection.news[index].isSaved;
              });
            },
          ))
      .toList();
}
