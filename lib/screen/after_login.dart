import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/controller.dart';
import 'package:web_dashboard_admin/session_global.dart';

import '../main_screen.dart';
import '../network_api.dart';

class AfterLoginScreen extends StatefulWidget {
  @override
  _AfterLoginScreenState createState() => _AfterLoginScreenState();
}

class _AfterLoginScreenState extends State<AfterLoginScreen> {
  final ProfileController profileController = Get.find();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) =>
      FocusDetector(
        onFocusLost: () {
          logger.i(
            'Focus Lost.'
                '\nTriggered when either [onVisibilityLost] or [onForegroundLost] '
                'is called.'
                '\nEquivalent to onPause() on Android or viewDidDisappear() on iOS.',
          );
        },
        onFocusGained: () {
          logger.i(
            'Focus Gained.'
                '\nTriggered when either [onVisibilityGained] or [onForegroundGained] '
                'is called.'
                '\nEquivalent to onResume() on Android or viewDidAppear() on iOS.',
          );

          callAPIProfile(context);
        },
        onVisibilityLost: () {
          logger.i(
            'Visibility Lost.'
                '\nIt means the widget is no longer visible within your app.',
          );
        },
        onVisibilityGained: () {
          logger.i(
            'Visibility Gained.'
                '\nIt means the widget is now visible within your app.',
          );
        },
        onForegroundLost: () {
          logger.i(
            'Foreground Lost.'
                '\nIt means, for example, that the user sent your app to the '
                'background by opening another app or turned off the device\'s '
                'screen while your widget was visible.',
          );
        },
        onForegroundGained: () {
          logger.i(
            'Foreground Gained.'
                '\nIt means, for example, that the user switched back to your app '
                'or turned the device\'s screen back on while your widget was visible.',
          );
        },
        child: Material(
          color: Colors.white,
          child: SizedBox(),
        ),
      );


  callAPIProfile(BuildContext context) async {
    // final ProfileController profileController = Get.put(ProfileController(),permanent: true);
    await profileController.callAPIGetProfile(context);
    // profileController.userProfile.value;
    checkWhereToGo(); //make sure after profileController completed
  }

  checkWhereToGo() {
    print("checkWhereToGo");
    Session.shared.goDashBoard(context);
  }

}
