import 'dart:typed_data';

import 'package:dio/dio.dart' as Dioo;
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/controller.dart';
import 'package:web_dashboard_admin/model/objects/listing_detail.dart';
import 'package:web_dashboard_admin/model/response/listing_add_response.dart';
import 'package:web_dashboard_admin/model/response/listing_list_response.dart';
import 'package:web_dashboard_admin/model/response/listing_replace_image_response.dart';
import 'package:web_dashboard_admin/network_api.dart';
import 'package:web_dashboard_admin/session_global.dart';
import 'package:web_dashboard_admin/url_app.dart';
import 'package:web_dashboard_admin/widget/dialogs/custom_dialog.dart';

class ListingController extends GetxController {
  var _listingListResponse;
  var _data;
  var isDone = false.obs;
  var urlRefresh = false.obs;
  var processing = false.obs;
  var total = 0.obs;

  ListingListResponse get listingListResponse => _listingListResponse;

  List<ListingDetail> get data => _data;

  BuildContext context;

  ListingController(this.context);

  @override
  void onInit() {
    getAllListing(context);
    super.onInit();
  }

  getAllListing(BuildContext context) async {
    isDone.value = false;
    var endpoint = url_listing_list;
    var jsonQuery = {
      // "account_id": Session.shared.HASH_ID,
      "account_id": 0,
      "access-token": await Session.shared.getToken(),
    };
    var network = NetworkAPI(endpoint: endpoint, jsonQuery: jsonQuery);
    var jsonBody = await network.callAPI(method: "GET", showLog: true);
    if (jsonBody["code"] == 100) {
      _listingListResponse = ListingListResponse.fromJson(jsonBody);
      _data = listingListResponse.data
          .where((element) => element.status != 4)
          .toList();
      total.value = data.length;
      isDone.value = true;
      update();
      final ProfileController profileController = Get.find();
      await profileController.callAPIGetProfile(context);
    } else {
      isDone.value = true;
      CustomDialog()
          .showAlertDialog(title: "Error", message: jsonBody["message"]);
    }
  }

  addListing(BuildContext context,
      {String title = "", String content = "", List<Uint8List> bytes}) async {
    processing.value = true;
    // Session.shared.showLoading();

    var formData = _getFormDataWithBytes(bytes, "test");
    formData.fields.addAll([
      MapEntry('title', title),
      MapEntry('content', content),
    ]);

    var jsonQuery = {"access-token": await Session.shared.getToken()};
    var network = NetworkAPI(
        endpoint: url_listing_add, formData: formData, jsonQuery: jsonQuery);
    var respondData = await network.callAPI(
         showLog: true,
        method: "POST",
        percent: (percent) {
          print(percent);
          if (percent != 1) {
            EasyLoading.showProgress(percent, status: 'Uploading..');
          } else {
            EasyLoading.dismiss(animation: true);
          }
        });

    print('respondData $respondData');
    // Session.shared.hideLoading();
    if (respondData['code'] == 100) {
      var mode = ListingAddResponse.fromJson(respondData);
      print(mode.toJson());
      listingListResponse.data.insert(0, mode.data);
      data.insert(0, mode.data);
      total.value = data.length;
      // update();
      final ProfileController profileController = Get.find();
      await profileController.callAPIGetProfile(context);
      processing.value = false;
      //after call sucess refresh list
      await getAllListing(context);
      Get.back();
    } else {
      print('error here $respondData');
      CustomDialog()
          .showAlertDialog(title: "Error", message: respondData['message']);
      processing.value = false;
    }
    //
  }

  editListing(BuildContext context,
      {String title = "", String content = "", @required int listingId}) async {
    processing.value = true;
    Session.shared.showLoading();

    Dioo.FormData formData = new Dioo.FormData.fromMap(
        {'title': title, 'content': content, 'listing_id': listingId});

    var jsonQuery = {"access-token": await Session.shared.getToken()};
    var network = NetworkAPI(
        endpoint: url_listing_edit, formData: formData, jsonQuery: jsonQuery);

    var jsonBody = await network.callAPI(
        method: "POST",
        percent: (percent) {
          print(percent);
          if (percent != 1) {
            EasyLoading.showProgress(percent, status: 'Editing..');
          } else {
            EasyLoading.dismiss(animation: true);
          }
        });

    Session.shared.hideLoading();
    if (jsonBody["code"] == 100) {
      var response = ListingAddResponse.fromJson(jsonBody);

      listingListResponse.data[listingListResponse.data
              .indexWhere((element) => element.listingId == listingId)] =
          response.data;
      data[data.indexWhere((element) => element.listingId == listingId)] =
          response.data;

      update();
      final ProfileController profileController = Get.find();
      await profileController.callAPIGetProfile(context);
      await getAllListing(context);
      processing.value = false;
    } else {

      CustomDialog()
          .showAlertDialog(title: "Error", message: jsonBody["message"]);
      processing.value = false;
    }
  }

  deleteListing(BuildContext context, {int listing_id}) async {
    Session.shared.showLoading();

    Dioo.FormData formData =
        new Dioo.FormData.fromMap({'listing_id': listing_id});
    var jsonQuery = {"access-token": await Session.shared.getToken()};
    var network = NetworkAPI(
        endpoint: url_listing_delete, formData: formData, jsonQuery: jsonQuery);
    var jsonBody = await network.callAPI(method: "POST");

    Session.shared.hideLoading();
    if (jsonBody["code"] == 100) {
      listingListResponse.data
          .removeWhere((element) => element.listingId == listing_id);
      data.removeWhere((element) => element.listingId == listing_id);
      total.value = data.length;
      update();
      final ProfileController profileController = Get.find();
      await profileController.callAPIGetProfile(context);
      await getAllListing(context);
    } else {
      CustomDialog()
          .showAlertDialog(title: "Error", message: jsonBody["message"]);
    }
  }

  updateImageListing(BuildContext context,
      {@required Uint8List byte,
      @required int galleryId,
      @required int listingId}) async {
    Dioo.FormData formData = new Dioo.FormData.fromMap({
      "imageFile": Dioo.MultipartFile.fromBytes(byte, filename: "_image.jpg"),
      'id': galleryId
    });
    Session.shared.showLoading();

    var jsonQuery = {"access-token": await Session.shared.getToken()};
    var network = NetworkAPI(
        endpoint: url_listing_replace_image,
        formData: formData,
        jsonQuery: jsonQuery);
    var jsonBody = await network.callAPI(
        method: "POST",
        percent: (percent) {
          print(percent);
          if (percent != 1) {
            EasyLoading.showProgress(percent, status: 'Uploading..');
          } else {
            EasyLoading.dismiss(animation: true);
          }
        });
    Session.shared.hideLoading();
    if (jsonBody["code"] == 100) {
      var response = ListingReplaceImageResponse.fromJson(jsonBody);
      for (final item in data) {
        if (item.listingId == listingId) {
          item.gallery[0].url = response.link;
          item.primaryPhoto = response.link;
          urlRefresh.value = !urlRefresh.value;
          // await getAllListing(context);
          update();
        }
      }
    } else {
      CustomDialog()
          .showAlertDialog(title: "Error", message: jsonBody["message"]);
    }
  }

  // add multi image body
  Dioo.FormData _getFormDataWithBytes(List<Uint8List> bytes, String fileName) {
    var multipartFiles = <Dioo.MultipartFile>[];
    var exampleName = 0;
    for (final byte in bytes) {
      multipartFiles.add(
          Dioo.MultipartFile.fromBytes(byte, filename: exampleName.toString()));
      exampleName++;
    }

    var formData = new Dioo.FormData.fromMap({
      "imageFiles[]": multipartFiles,
    });
    return formData;
  }
}
