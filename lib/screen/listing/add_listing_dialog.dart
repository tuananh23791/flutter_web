import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:web_dashboard_admin/constants.dart';
import 'package:web_dashboard_admin/model/objects/listing_detail.dart';
import 'package:web_dashboard_admin/screen/listing/listing_controller.dart';
import 'package:web_dashboard_admin/size_config.dart';
import 'package:web_dashboard_admin/utils.dart';
import 'package:web_dashboard_admin/widget/common/default_button.dart';
import 'package:web_dashboard_admin/widget/dialogs/custom_dialog.dart';

class AddListingDialog extends StatefulWidget {
  const AddListingDialog({Key key}) : super(key: key);

  @override
  _AddListingDialogState createState() => _AddListingDialogState();

  static show(BuildContext context) {
    Utils.shared.dialog(context, AddListingDialog());
  }
}

class _AddListingDialogState extends State<AddListingDialog> {
  final ListingController listingController = Get.find<ListingController>();
  List<Uint8List> bytes = <Uint8List>[];

  TextEditingController textControllerTitle;
  FocusNode textFocusNodeTitle;

  TextEditingController textControllerContent;
  FocusNode textFocusNodeContent;

  bool iconHovered = false;
  bool imageHovered = false;

  @override
  void initState() {
    textControllerTitle = TextEditingController();
    textControllerContent = TextEditingController();
    textFocusNodeTitle = FocusNode();
    textFocusNodeContent = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    textControllerContent.dispose();
    textControllerTitle.dispose();
    textFocusNodeTitle.dispose();
    textFocusNodeContent.dispose();
    super.dispose();
  }

  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  _onHover(PointerEvent details, int type) {
    setStateIfMounted(() {
      switch (type) {
        case 1:
          imageHovered = true;
          break;
        case 2:
          iconHovered = true;
          break;
      }
    });
  }

  _onExit(PointerEvent details, int type) {
    setStateIfMounted(() {
      switch (type) {
        case 1:
          imageHovered = false;
          break;
        case 2:
          iconHovered = false;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                  child: Card(
                    elevation: 3,
                    child: GestureDetector(
                      onTap: () {
                        _pickImages();
                      },
                      child: MouseRegion(
                        cursor: SystemMouseCursors.click,
                        onHover: (event) {
                          _onHover(event, 1);
                        },
                        onExit: (event) {
                          _onExit(event, 1);
                        },
                        child: bytes.isEmpty
                            ? SizedBox(
                                width: 200,
                                height: 200,
                                child: Icon(Icons.upload_rounded, size: 24),
                              )
                            : Image.memory(
                                bytes[0],
                                fit: BoxFit.cover,
                                height: 200,
                                width: 200,
                              ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    focusNode: textFocusNodeTitle,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    controller: textControllerTitle,
                    autofocus: false,
                    onChanged: (value) {},
                    onSubmitted: (value) {
                      textFocusNodeTitle.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeContent);
                    },
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      hintText: "Title",
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    minLines: 4,
                    maxLines: 6,
                    focusNode: textFocusNodeContent,
                    keyboardType: TextInputType.multiline,
                    textInputAction: TextInputAction.done,
                    controller: textControllerContent,
                    autofocus: false,
                    onChanged: (value) {},
                    onSubmitted: (value) {
                      textFocusNodeContent.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeContent);
                    },
                    style: TextStyle(color: Colors.black, fontSize: 14),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      hintText: "Content",
                    ),
                  ),
                ),
                SizedBox(height: 20),
               
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Obx(
                    () => DefaultButton(
                      text: "Save",
                      enableLoading: true,
                      showLoading: listingController.processing.value,
                      press: _addListing,
                      color: kPrimaryColor,
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _pickImages() async {
    print("_pickImages");
    FilePickerResult result = await FilePicker.platform
        .pickFiles(type: FileType.image, allowMultiple: true);
    if (result != null) {
      if (result.files.isNotEmpty) bytes.clear();

      for (var file in result.files) {
        bytes.add(file.bytes);
      }
      setState(() {});
      if (kIsWeb) {
      } else {
        //do something for other platform
      }
    } else {
      // User canceled the picker
    }
  }

  _addListing() async {
    var msg = validData();
    if (msg.isNotEmpty) {
      CustomDialog().showAlertDialog(title: "Alert", message: msg);
    } else {
      await listingController.addListing(context,
          bytes: bytes,
          title: textControllerTitle.text,
          content: textControllerContent.text);
      //after call api refresh list

      // Get.back();
    }
  }

  String validData() {
    var msg = "";
    if (textControllerTitle.text.isEmpty) {
      return "Title can't blank";
    } else if (textControllerContent.text.isEmpty) {
      return "Content can't blank";
    } else if (bytes.isEmpty) {
      return "Please choose images";
    }
    return msg;
  }
}
