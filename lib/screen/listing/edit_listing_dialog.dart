import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:web_dashboard_admin/constants.dart';
import 'package:web_dashboard_admin/model/objects/listing_detail.dart';
import 'package:web_dashboard_admin/screen/listing/listing_controller.dart';
import 'package:web_dashboard_admin/size_config.dart';
import 'package:web_dashboard_admin/utils.dart';
import 'package:web_dashboard_admin/widget/common/default_button.dart';

class EditListingDialog extends StatefulWidget {
  final ListingDetail listingDetail;

  const EditListingDialog({Key key, @required this.listingDetail})
      : super(key: key);

  @override
  _EditListingDialogState createState() => _EditListingDialogState();

  static show(BuildContext context, {@required ListingDetail listingDetail}) {
    Utils.shared.dialog(
        context,
        EditListingDialog(
          listingDetail: listingDetail,
        ));
  }
}

class _EditListingDialogState extends State<EditListingDialog> {
  final ListingController listingController = Get.find<ListingController>();
  final _picker = ImagePicker();

  TextEditingController textControllerTitle;
  FocusNode textFocusNodeTitle;

  TextEditingController textControllerContent;
  FocusNode textFocusNodeContent;

  bool iconHovered = false;
  bool imageHovered = false;

  @override
  void initState() {
    textControllerTitle = TextEditingController();
    textControllerContent = TextEditingController();
    textFocusNodeTitle = FocusNode();
    textFocusNodeContent = FocusNode();
    textControllerTitle.text = widget.listingDetail.title;
    textControllerContent.text = widget.listingDetail.content;
    super.initState();
  }

  @override
  void dispose() {
    textControllerContent.dispose();
    textControllerTitle.dispose();
    textFocusNodeTitle.dispose();
    textFocusNodeContent.dispose();
    super.dispose();
  }

  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  _onHover(PointerEvent details, int type) {
    setStateIfMounted(() {
      switch (type) {
        case 1:
          imageHovered = true;
          break;
        case 2:
          iconHovered = true;
          break;
      }
    });
  }

  _onExit(PointerEvent details, int type) {
    setStateIfMounted(() {
      switch (type) {
        case 1:
          imageHovered = false;
          break;
        case 2:
          iconHovered = false;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                  child: MouseRegion(
                    onHover: (event) {
                      _onHover(event, 1);
                    },
                    onExit: (event) {
                      _onExit(event, 1);
                    },
                    child: Stack(
                      children: [
                        MouseRegion(
                          child: Obx(() => CachedNetworkImage(
                                imageUrl: listingController.urlRefresh.value
                                    ? widget.listingDetail.primaryPhoto
                                    : widget.listingDetail.primaryPhoto,
                                height: 200,
                                width: 200,
                                fit: BoxFit.cover,
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) =>
                                    Center(child: Icon(Icons.error)),
                              )),
                        ),
                        imageHovered
                            ? Positioned(
                                child: MouseRegion(
                                  cursor: SystemMouseCursors.click,
                                  child: GestureDetector(
                                    onTap: _pickImage,
                                    child: Container(
                                        padding: EdgeInsets.all(2),
                                        decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.7),
                                          borderRadius:
                                              BorderRadius.circular(6),
                                        ),
                                        child: Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                          size: 12,
                                        )),
                                  ),
                                ),
                                right: 4,
                                top: 4)
                            : SizedBox(),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    focusNode: textFocusNodeTitle,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    controller: textControllerTitle,
                    autofocus: false,
                    onChanged: (value) {},
                    onSubmitted: (value) {
                      textFocusNodeTitle.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeContent);
                    },
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      hintText: "Title",
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    minLines: 4,
                    maxLines: 6,
                    focusNode: textFocusNodeContent,
                    keyboardType: TextInputType.multiline,
                    textInputAction: TextInputAction.done,
                    controller: textControllerContent,
                    autofocus: false,
                    onChanged: (value) {},
                    onSubmitted: (value) {
                      textFocusNodeContent.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeContent);
                    },
                    style: TextStyle(color: Colors.black, fontSize: 14),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      hintText: "Content",
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Obx(
                    () => DefaultButton(
                      text: "Save",
                      enableLoading: true,
                      showLoading: listingController.processing.value,
                      press: _editListing,
                      color: kPrimaryColor,
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _pickImage() async {
    final pickedFile = await _picker.getImage();
    if (kIsWeb) {
      Uint8List byte = await pickedFile.readAsBytes();
      // Get.back();
      listingController.updateImageListing(context,
          byte: byte,
          galleryId: widget.listingDetail.gallery[0].id,
          listingId: widget.listingDetail.listingId);
    } else {
      //do something for other platform
    }
  }

  _editListing() async {
    await listingController.editListing(context,
        listingId: widget.listingDetail.listingId,
        title: textControllerTitle.text,
        content: textControllerContent.text);
    Get.back();
  }
}
