import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/constants.dart';
import 'package:web_dashboard_admin/screen/listing/add_listing_dialog.dart';
import 'package:web_dashboard_admin/screen/listing/list_pagination.dart';
import 'package:web_dashboard_admin/screen/listing/listing_card.dart';
import 'package:web_dashboard_admin/screen/listing/listing_controller.dart';
import 'package:web_dashboard_admin/widget/common/default_button.dart';

class ListingScreen extends StatefulWidget {
  @override
  _ListingScreenState createState() => _ListingScreenState();
}

class _ListingScreenState extends State<ListingScreen> {
  final ListingController listingController = Get.find<ListingController>();
  var _currentPage = 0.obs;
  var _itemCount = 2;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          width: 100,
          child: DefaultButton(
            text: "Add Listing",
            press: () {
              AddListingDialog.show(context);
            },
            color: kPrimaryColor,
            style: TextStyle(
                fontSize: 13,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 50,// nên thêm vào để cò cái heigt
          alignment: Alignment.centerRight,
          child: Obx(() => ListPagination(
            onTap: (currentPage) {
              _currentPage.value = currentPage;
              // setState(() {
              //   _currentPage = currentPage;
              // });
            },
            itemCount: _itemCount,
            totalItem: listingController.total.value,
          )),
        ),
        SizedBox(
          height: 10,
        ),

        Expanded(
          child: Obx(() {
            return _buildListingList();
          }),
        )

      ],
      ),
    );
  }

  Widget _buildListingList() {
    return listingController.data == null && !listingController.isDone.value
        ? SizedBox.expand(child: Center(child: CircularProgressIndicator()))
        : listingController.data == null && listingController.isDone.value
            ? SizedBox()
            : Container(
                child: ListView.builder(
                  itemCount: getItemCount(),
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: ListingCard(
                          listingDetail:
                              listingController.data[getItemIndex(index)]),
                    );
                  },
                ),
              );
  }

  getItemCount() {
    if (listingController.data.length - ((_currentPage) * _itemCount) >=
        _itemCount) {
      return _itemCount;
    } else {
      return listingController.data.length - ((_currentPage) * _itemCount);
    }
  }

  getItemIndex(int index) {
    if (_currentPage == 0) {
      return index;
    } else {
      var i = index + (_currentPage * _itemCount);
      return i;
    }
  }
}
