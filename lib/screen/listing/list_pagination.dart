import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:web_dashboard_admin/widget/common/action_icon.dart';

class ListPagination extends StatefulWidget {
  final int itemCount;
  final int totalItem;
  final Function(int) onTap;

  const ListPagination(
      {Key key,
      this.itemCount = 1,
      @required this.onTap,
      @required this.totalItem})
      : super(key: key);

  @override
  _ListPaginationState createState() => _ListPaginationState();
}

class _ListPaginationState extends State<ListPagination> {
  var pages = <int>[];
  int _currentPage;
  static const int previous = 0;
  static const int next = -1;

  @override
  void initState() {
    _currentPage = 0;
    super.initState();
  }

  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  @override
  Widget build(BuildContext context) {
    var totalPage = widget.totalItem % widget.itemCount == 0
        ? widget.totalItem / widget.itemCount
        : (widget.totalItem / widget.itemCount).floor() + 1;
    pages.clear();
    pages.add(previous);
    for (var i = 1; i <= totalPage; i++) {
      pages.add(i);
    }
    pages.add(next);
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: pages.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return pages[index] == previous
            ? ActionIcon(
                Icons.skip_previous,
                color: _currentPage == 0 ? Colors.grey : Colors.black,
                onTap: () {
                  if (_currentPage == 0) return;
                  setStateIfMounted(() {
                    _currentPage -= 1;
                  });
                  widget.onTap(_currentPage);
                },
              )
            : pages[index] == next
                ? ActionIcon(
                    Icons.skip_next,
                    color: _currentPage == totalPage - 1
                        ? Colors.grey
                        : Colors.black,
                    onTap: () {
                      if (_currentPage == totalPage - 1) return;
                      setStateIfMounted(() {
                        _currentPage += 1;
                      });
                      widget.onTap(_currentPage);
                    },
                  )
                : MouseRegion(
                    cursor: SystemMouseCursors.click,
                    child: GestureDetector(
                      onTap: () {
                        print("index : ${index.toString()} -- value : ${index-1}");
                        if (_currentPage == index - 1) return;
                        setStateIfMounted(() {
                          _currentPage = index - 1;
                        });
                        widget.onTap(_currentPage);
                      },
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 2),
                          child: Text(
                            '${pages[index].toString()}',
                            style: TextStyle(
                                color: _currentPage == index - 1
                                    ? Colors.blue
                                    : Colors.black),
                          ),
                        ),
                      ),
                    ),
                  );
      },
    );
  }
}
