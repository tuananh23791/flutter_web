import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/model/objects/listing_detail.dart';
import 'package:web_dashboard_admin/screen/listing/edit_listing_dialog.dart';
import 'package:web_dashboard_admin/screen/listing/listing_controller.dart';
import 'package:web_dashboard_admin/widget/common/action_icon.dart';

class ListingCard extends StatefulWidget {
  final ListingDetail listingDetail;

  const ListingCard({Key key, @required this.listingDetail}) : super(key: key);

  @override
  _ListingCardState createState() => _ListingCardState();
}

class _ListingCardState extends State<ListingCard> {
  final ListingController postController = Get.find<ListingController>();
  bool isHovered = false;

  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  _onHover(PointerEvent details) {
    setStateIfMounted(() {
      isHovered = true;
    });
  }

  _onExit(PointerEvent details) {
    setStateIfMounted(() {
      isHovered = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onHover: _onHover,
      onExit: _onExit,
      child: Card(
        elevation: isHovered ? 3 : 1,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CachedNetworkImage(
                      imageUrl: widget.listingDetail.primaryPhoto,
                      height: 75,
                      width: 75,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) =>
                          Center(child: Icon(Icons.error)),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${widget.listingDetail.title}",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                            ),
                            SizedBox(height: 10),
                            Text(
                              "${widget.listingDetail.content}",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.normal),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ActionIcon(
                Icons.edit,
                enableHover: true,
                onTap: () {
                  EditListingDialog.show(context,
                      listingDetail: widget.listingDetail);
                },
              ),
              ActionIcon(
                Icons.delete,
                enableHover: true,
                onTap: () {
                  postController.deleteListing(context,
                      listing_id: widget.listingDetail.listingId);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
