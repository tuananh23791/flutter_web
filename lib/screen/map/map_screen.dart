import 'dart:async';

import 'package:flutter/material.dart';
//import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();

//  double _originLatitude = 26.48424, _originLongitude = 50.04551;
//  double _destLatitude = 26.46423, _destLongitude = 50.06358;
//  Map<MarkerId, Marker> markers = {};
//  Map<PolylineId, Polyline> polylines = {};
//  List<LatLng> polylineCoordinates = [];
//  PolylinePoints polylinePoints = PolylinePoints();
//  String googleAPiKey = "";

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//
//
//    /// origin marker
//    _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//        BitmapDescriptor.defaultMarker);
//
//    /// destination marker
//    _addMarker(LatLng(_destLatitude, _destLongitude), "destination",
//        BitmapDescriptor.defaultMarkerWithHue(90));
//    _getPolyline();
//  }
  @override
  Widget build(BuildContext context) {
//    return new Scaffold(
//      body: GoogleMap(
//        initialCameraPosition: CameraPosition(
//            target: LatLng(_originLatitude, _originLongitude), zoom: 15),
//        myLocationEnabled: true,
//        tiltGesturesEnabled: true,
//        compassEnabled: true,
//        scrollGesturesEnabled: true,
//        zoomGesturesEnabled: true,
//        markers: Set<Marker>.of(markers.values),
//        polylines: Set<Polyline>.of(polylines.values),
//        onMapCreated: (GoogleMapController controller) {
//          _controller.complete(controller);
//        },
//      ),
//      floatingActionButton: FloatingActionButton.extended(
//        onPressed: _goToTheLake,
//        label: Text('To the lake!'),
//        icon: Icon(Icons.directions_boat),
//      ),
//    );

    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.hybrid,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToTheLake,
        label: Text('To the lake!'),
        icon: Icon(Icons.directions_boat),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }



//  _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
//    MarkerId markerId = MarkerId(id);
//    Marker marker =
//    Marker(markerId: markerId, icon: descriptor, position: position);
//    markers[markerId] = marker;
//  }

//  _addPolyLine() {
//    PolylineId id = PolylineId("poly");
//    Polyline polyline = Polyline(
//        polylineId: id, color: Colors.red, points: polylineCoordinates);
//    polylines[id] = polyline;
//    setState(() {});
//  }

//  _getPolyline() async {
//    print("11111");
//    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//        googleAPiKey,
//        PointLatLng(_originLatitude, _originLongitude),
//        PointLatLng(_destLatitude, _destLongitude),
//        travelMode: TravelMode.driving,
//        wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]);
//    print("22222222");
//    if (result.points.isNotEmpty) {
//      result.points.forEach((PointLatLng point) {
//        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//      });
//    }
//    print("333333");
//    _addPolyLine();
//    print("4444444");
//    for(var v in polylines.values) {
//      print(":::::$v");
//      //below is the solution
////      v.asMap().forEach((i, value) {
////        print('index=$i, value=$value');
////      }
//          }
//  }
}
