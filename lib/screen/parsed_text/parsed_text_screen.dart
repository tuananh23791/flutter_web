import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:web_dashboard_admin/widget/dialogs/custom_dialog.dart';

class ParsedTextScreen extends StatefulWidget {
  @override
  _ParsedTextScreenState createState() => _ParsedTextScreenState();
}

class _ParsedTextScreenState extends State<ParsedTextScreen> {
  TextEditingController myController;

  @override
  void initState() {
    myController = TextEditingController();
    myController.text =
        "Hello #BWM website http://bwm.com mail bwm@bwm.bwm #CAR #SPORT !";
    myController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          TextField(
            controller: myController,
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Enter something'),
          ),
          ParsedText(
            text: myController.text,
            parse: <MatchText>[
              MatchText(
                type: ParsedType.EMAIL,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 24,
                ),
                onTap: (email) {
                  CustomDialog()
                      .showAlertDialog(title: "", message: email);
                },
              ),
              MatchText(
                type: ParsedType.URL,
                style: TextStyle(
                  color: Colors.yellow,
                  fontSize: 24,
                ),
                onTap: (url) {
                  CustomDialog()
                      .showAlertDialog(title: "", message: url);
                },
              ),
              MatchText(
                pattern: r"\B(\#[a-zA-Z0-9]+\b)(?!;)",// #hashtag regex
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 24,
                ),
                onTap: (tag) {
                  CustomDialog()
                      .showAlertDialog(title: "", message: tag);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
