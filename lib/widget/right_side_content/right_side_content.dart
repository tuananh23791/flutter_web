import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/model/drawer_menu_item.dart';
import 'package:web_dashboard_admin/model/drawer_notification_item.dart';
import 'package:web_dashboard_admin/model/popup_menu_item.dart';
import 'package:web_dashboard_admin/responsive.dart';
import 'package:web_dashboard_admin/screen/carousel_slider/vertical_slider_demo.dart';
import 'package:web_dashboard_admin/screen/chart/chart_screen.dart';
import 'package:web_dashboard_admin/screen/listing/listing_controller.dart';
import 'package:web_dashboard_admin/screen/listing/listing_screen.dart';
import 'package:web_dashboard_admin/screen/form/form_screen.dart';
import 'package:web_dashboard_admin/screen/login_screen.dart';
import 'package:web_dashboard_admin/screen/map/map_screen.dart';
import 'package:web_dashboard_admin/screen/parsed_text/parsed_text_screen.dart';
import 'package:web_dashboard_admin/screen/pdf/pdf_screen.dart';
import 'package:web_dashboard_admin/screen/silver_tools/custom_silvers_page.dart';
import 'package:web_dashboard_admin/screen/silver_tools/news_controller.dart';
import 'package:web_dashboard_admin/screen/timer/circular_countdown_timer_screen.dart';
import 'package:web_dashboard_admin/screen/search_list/search_list_screen.dart';
import 'package:web_dashboard_admin/screen/upload_file/upload_file_controller.dart';
import 'package:web_dashboard_admin/screen/upload_file/upload_file_screen.dart';
import 'package:web_dashboard_admin/session_global.dart';
import 'package:web_dashboard_admin/side_menu.dart';
import 'package:web_dashboard_admin/utils.dart';
import 'package:web_dashboard_admin/widget/dialogs/popup_menu_button_custom.dart';
import 'package:web_dashboard_admin/widget/dialogs/popup_message.dart';
import 'package:web_dashboard_admin/widget/drawers/drawer_notification/drawer_animation_custom.dart';
import 'package:web_dashboard_admin/widget/grid_list/grid_view_custom.dart';

import '../slide_menu/item_slide_menu_controller.dart';

class RightSideContent extends StatefulWidget {
  @override
  _RightSideContentState createState() => _RightSideContentState();
}

class _RightSideContentState extends State<RightSideContent> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        endDrawerEnableOpenDragGesture: false,
        endDrawer: DrawerAnimationCustom(
          options: drawerNotificationItemDemo(),
        ),
        drawer: Responsive.isMobile(context)
            ? Drawer(
                //dùg để check 1 case cụ thể nào đó nếu thoả
                child: SideMenu(),
              )
            : null,
        appBar: AppBar(
          title:
              Obx(() => Text(Get.find<ItemSlideMenuController>().title.value)),
          actions: [
            IconButton(
                icon: Icon(Icons.notifications), onPressed: _openEndDrawer),
            PopupMenuButtonCustom(
              options: popupMenuItemDemo(),
              onItemSelected: (selectedItem) {
                print(selectedItem.value);
              },
            ),
            IconButton(
                icon: Icon(Icons.logout),
                onPressed: () {
                  showPopupAskLogout(context);
                }),
          ],
        ),
        body: Obx(() =>
            getBody(type: Get.find<ItemSlideMenuController>().type.value)));
  }

  Widget getBody({int type = 0}) {
    switch (type) {
      case 4:
        return PDFScreen();
      case 5:
        return ParsedTextScreen();
      case 6:
        return CircularCountdownTimerScreen();
      case 8:
        Get.put(UploadFileController());
        return UploadFileScreen();
      case 7:
        Get.put(ListingController(context));
        return ListingScreen();
      case 9:
        return FormScreen();
      case 10:
        return MapScreen();
      case 12:
        return SearchListScreen();

      case 13:
        Get.put(NewsController());
        return CustomSilversPage();

      case 14:
        return VerticalSliderDemo();

      case 15:
        return ChartScreen();
      default:
        return gridView();
    }
  }

  Widget gridView() {
    return Container(
      child: Responsive.isDesktop(context)
          ? GridViewCustom(
              options: ['1', '2', '3', '4', '5'], crossAxisCount: 3)
          : Responsive.isTablet(context)
              ? GridViewCustom(
                  options: ['1', '2', '3', '4', '5'], crossAxisCount: 2)
              : GridViewCustom(
                  options: ['1', '2', '3', '4', '5'], crossAxisCount: 1),
    );
  }

  void _openEndDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void _closeEndDrawer() {
    Utils.shared.pop(context);
  }

  void showPopupAskLogout(BuildContext context) {
    PopupMessage.show(context, "LOGOUT", "Are you sure you want to exit !!!",
        awareMessage: "YES", onAware: () => logout(context));
  }

  Future<void> logout(BuildContext context) async {
    // controller handle
    Session.shared.showLoading();
    await Session.shared.removeToken();
    Session.shared.hideLoading();

    Utils.shared.changeScreenAndRemove(context, LoginScreen());
  }

  List<PopupMenuItemCustom> popupMenuItemDemo() {
    var options = <PopupMenuItemCustom>[];

    options.add(PopupMenuItemCustom(
        menuItemEnums: PopupMenuItemEnums.DEFAULT, value: "SUN"));
    options.add(PopupMenuItemCustom(
        menuItemEnums: PopupMenuItemEnums.DEFAULT, value: "MON"));
    options.add(PopupMenuItemCustom(
        menuItemEnums: PopupMenuItemEnums.WITH_ICON, value: "TUE"));
    options.add(PopupMenuItemCustom(
        menuItemEnums: PopupMenuItemEnums.DEFAULT, value: "WED"));

    return options;
  }

  List<DrawerMenuItemCustom> drawerMenuItemDemo() {
    var options = <DrawerMenuItemCustom>[];

    options.add(DrawerMenuItemCustom(
        drawerMenuItemEnums: DrawerMenuItemEnums.DEFAULT,
        value: "Profile",
        icon: Icon(Icons.person),
        onTap: (value) {
          print(value);
        }));

    options.add(DrawerMenuItemCustom(
        drawerMenuItemEnums: DrawerMenuItemEnums.DEFAULT,
        value: "New Chat",
        icon: Icon(Icons.chat),
        onTap: (value) {
          print(value);
        }));

    options.add(DrawerMenuItemCustom(
        drawerMenuItemEnums: DrawerMenuItemEnums.WITH_DIVIDER,
        value: "Call",
        icon: Icon(Icons.call),
        onTap: (value) {
          print(value);
        }));

    options.add(DrawerMenuItemCustom(
        drawerMenuItemEnums: DrawerMenuItemEnums.CLOSE,
        value: "Close",
        icon: Icon(Icons.close),
        onTap: (value) {}));

    return options;
  }

  List<DrawerNotificationItemExample> drawerNotificationItemDemo() {
    var options = <DrawerNotificationItemExample>[];

    options.add(DrawerNotificationItemExample(
      drawerNotificationEnums: DrawerNotificationEnums.ACTIONS,
      value: "Alerts",
      icon: Icon(Icons.person),
      onTap: (value) {
        print(value);
      },
    ));

    options.add(DrawerNotificationItemExample(
      drawerNotificationEnums: DrawerNotificationEnums.MENU_OPTIONS,
      value: "All projects",
      icon: Icon(Icons.person),
      onTap: (value) {
        print(value);
      },
    ));

    options.add(DrawerNotificationItemExample(
      drawerNotificationEnums: DrawerNotificationEnums.DEFAULT,
      value: "Warning 1",
      icon: Icon(Icons.person),
      onTap: (value) {
        print(value);
      },
    ));

    options.add(DrawerNotificationItemExample(
      drawerNotificationEnums: DrawerNotificationEnums.DEFAULT,
      value: "Warning 2",
      icon: Icon(Icons.person),
      onTap: (value) {
        print(value);
      },
    ));

    options.add(DrawerNotificationItemExample(
      drawerNotificationEnums: DrawerNotificationEnums.DEFAULT,
      value: "Warning 3",
      icon: Icon(Icons.person),
      onTap: (value) {
        print(value);
      },
    ));
    return options;
  }
}
