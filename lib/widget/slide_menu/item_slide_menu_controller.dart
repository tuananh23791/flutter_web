import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/widget/slide_menu/item_slide_menu.dart';

class ItemSlideMenuController extends GetxController{
  var idItemSelected = 0.obs;
  var title = "".obs;
  var type = 0.obs;
  ItemSlideMenuState itemSlideMenuState;
}