import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/widget/slide_menu/badge_widget.dart';
import 'package:web_dashboard_admin/widget/slide_menu/item_slide_menu_controller.dart';

class ItemSlideMenu extends StatefulWidget {
  final List<ItemSlideMenu> children;
  final String text;
  final Widget icon;
  final Color backgroundColor;
  final Color textColor;
  final Badge badge;
  final Function(String) onTap;
  final EdgeInsets padding;

  const ItemSlideMenu(this.text,
      {Key key,
      this.children,
      this.icon,
      this.backgroundColor,
      this.textColor,
      this.badge, this.onTap, this.padding})
      : super(key: key);

  @override
  ItemSlideMenuState createState() => ItemSlideMenuState();
}

class ItemSlideMenuState extends State<ItemSlideMenu> {
  final Color _defaultColor = Color.fromARGB(255, 27, 35, 48);
  final Color _colorHover = Color.fromARGB(255, 37, 47, 62);
  Color _colorOfItem;
  int _idItem;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _colorOfItem = widget.backgroundColor ?? _defaultColor;
    _idItem = Get.find<ItemSlideMenuController>().idItemSelected.value;
    Get.find<ItemSlideMenuController>().idItemSelected.value++;
  }

  _onHover(PointerEvent details) {
    setStateIfMounted(() {
      _colorOfItem = _colorHover;
    });
  }

  _onExit(PointerEvent details) {
    if (Get.find<ItemSlideMenuController>().idItemSelected.value == _idItem)
      return;

    setStateIfMounted(() {
      _colorOfItem = widget.backgroundColor ?? _defaultColor;
    });
  }

  onRemoveSelected() {
    setStateIfMounted(() {
      _colorOfItem = widget.backgroundColor ?? _defaultColor;
    });
  }

  _onSelected() {
    if (Get.find<ItemSlideMenuController>().itemSlideMenuState != null) {
      Get.find<ItemSlideMenuController>().itemSlideMenuState.onRemoveSelected();
    }
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onHover: _onHover,
      onExit: _onExit,
      child: (widget.children != null && widget.children.length > 0)
          ? _groupItem()
          : _cardItem(),
    );
  }
  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  Widget _groupItem() {
    return Theme(
        data: Theme.of(context).copyWith(
            accentColor: Colors.white,
            unselectedWidgetColor: Colors.white),
        child: ExpansionTile(
          title: Text(widget.text,
              style: TextStyle(
                color: widget.textColor ?? Colors.white, fontSize: 12,
              )),
          leading: Container(
            child: widget.icon ?? SizedBox(),
            width: 30,
            height: 30,
          ),
          backgroundColor: widget.backgroundColor ?? _defaultColor,
          collapsedBackgroundColor: widget.backgroundColor ?? _defaultColor,
          children: widget.children,
        ));
  }

  Widget _cardItem() {
    return GestureDetector(
      onTap: () {
        Get.find<ItemSlideMenuController>().idItemSelected.value = _idItem;
        _onSelected();
        Get.find<ItemSlideMenuController>().itemSlideMenuState = this;
        setStateIfMounted(() {
          _colorOfItem = _colorHover;
        });
        widget.onTap(widget.text);
      },
      child: Container(
        padding: widget.padding,
        color: _colorOfItem,
        child: Card(
          color: _colorOfItem,
          shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.zero,
          ),
          borderOnForeground: true,
          elevation: 0,
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: ListTile(
            mouseCursor: SystemMouseCursors.click,
            leading: Container(
              child: widget.icon ?? SizedBox(),
              width: 30,
              height: 30,
            ),
            title: Text(
              widget.text ?? "",
              style: TextStyle(
                  fontSize: 12, color: widget.textColor ?? Colors.white),
            ),
            trailing: widget.badge ?? SizedBox(),
          ),
        ),
      ),
    );
  }
}
