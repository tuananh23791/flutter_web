import 'package:flutter/material.dart';

class Badge extends StatelessWidget {
  final int number;
  final Color backgroundColor;

  const Badge({Key key, this.number = 0, this.backgroundColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return number == 0
        ? SizedBox()
        : Container(
            padding: EdgeInsets.fromLTRB(5,2,5,2),
            decoration: BoxDecoration(
                color: backgroundColor??Colors.red,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Text("$number",style: TextStyle(color: Colors.white),),
          );
  }
}
