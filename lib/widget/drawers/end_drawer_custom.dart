import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/model/drawer_menu_item.dart';

class EndDrawerCustom extends StatefulWidget {
  final List<DrawerMenuItemCustom> options;

  const EndDrawerCustom({Key key, @required this.options}) : super(key: key);

  @override
  _EndDrawerCustomState createState() => _EndDrawerCustomState();
}

class _EndDrawerCustomState extends State<EndDrawerCustom> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: widget.options.length,
        itemBuilder: (context, i) {
          return Column(
            children: [
              getListTileByType(
                  drawerMenuItemCustom: widget.options[i], context: context),
              if (widget.options[i].drawerMenuItemEnums ==
                  DrawerMenuItemEnums.WITH_DIVIDER)
                Divider(),
            ],
          );
        },
      ),
    );
  }

  getListTileByType({
    @required DrawerMenuItemCustom drawerMenuItemCustom,
    BuildContext context,
  }) {
    switch (drawerMenuItemCustom.drawerMenuItemEnums) {
      case DrawerMenuItemEnums.CLOSE:
        return ListTile(
            title: Text(drawerMenuItemCustom.value ?? ""),
            leading: drawerMenuItemCustom.icon ?? Icon(Icons.close),
            onTap: () {
              Navigator.of(context).pop();
            });
        break;
      case DrawerMenuItemEnums.WITH_DIVIDER:
      case DrawerMenuItemEnums.DEFAULT:
        return ListTile(
            title: Text(drawerMenuItemCustom.value ?? ""),
            leading: drawerMenuItemCustom.icon ?? Icon(Icons.close),
            onTap: () {
              drawerMenuItemCustom.onTap(drawerMenuItemCustom.value);
            });
    }
  }
}
