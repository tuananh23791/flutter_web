import "dart:math" show pi;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:web_dashboard_admin/model/drawer_notification_item.dart';
import 'package:web_dashboard_admin/model/drop_down_item_example.dart';
import 'package:web_dashboard_admin/utils.dart';
import 'package:web_dashboard_admin/widget/dialogs/dropdown/drop_down_custom.dart';
import 'package:web_dashboard_admin/widget/drawers/drawer_notification/drawer_notification_card.dart';

class DrawerItemAnimationCustom extends StatefulWidget {
  final int index;
  final DrawerNotificationItemExample drawerNotificationItem;
  final AnimationController animationController;
  final double duration;
  final Function(DrawerNotificationItemExample) onDeleteCard;

  DrawerItemAnimationCustom(
      {this.index,
      this.animationController,
      this.duration,
      this.drawerNotificationItem, this.onDeleteCard});

  @override
  _DrawerItemAnimationCustomState createState() =>
      _DrawerItemAnimationCustomState();
}

class _DrawerItemAnimationCustomState extends State<DrawerItemAnimationCustom> {
  Animation _animation;
  double start;
  double end;
  Animation<double> rotateY;

  @override
  void initState() {
    super.initState();
    start = (widget.duration * widget.index).toDouble();
    end = start + widget.duration;
    _animation = Tween<double>(
      begin: 1.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: widget.animationController,
        curve: Interval(
          widget.index == 0 ? start : start - widget.duration / 2,
          widget.index == 0 ? end + widget.duration : end + widget.duration / 2,
          curve: Curves.easeIn,
        ),
      ),
    )..addListener(() {
        setState(() {});
      });

    rotateY = new Tween<double>(
      begin: -0.5,
      end: .0,
    ).animate(
      CurvedAnimation(
        parent: widget.animationController,
        curve: Interval(
          widget.index == 0 ? start : start - widget.duration / 2,
          widget.index == 0 ? end + widget.duration : end + widget.duration / 2,
          curve: Curves.easeIn,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new AnimatedBuilder(
      animation: widget.animationController,
      builder: (context, child) {
        final card = Opacity(
          opacity: _animation.value,
          child: getWidgetByType(
              drawerNotificationItem: widget.drawerNotificationItem,
              context: context),
        );

        return new Transform(
          transform: new Matrix4.rotationY(rotateY.value * pi),
          alignment: Alignment.centerLeft,
          child: card,
        );
      },
    );
  }

  getWidgetByType({
    @required DrawerNotificationItemExample drawerNotificationItem,
    BuildContext context,
  }) {
    switch (drawerNotificationItem.drawerNotificationEnums) {
      case DrawerNotificationEnums.DEFAULT:
        return DrawerNotificationCard(
          itemExample: drawerNotificationItem,
          onTap: (itemSelected) {
            print('selected : ${itemSelected.value}');
            // call back to parent
          },
          onDelete: (itemDeleted) {
            print('deleted : ${itemDeleted.value}');
            widget.onDeleteCard(itemDeleted);
          },
        );
        break;
      case DrawerNotificationEnums.ACTIONS:
        return Container(
          padding: EdgeInsets.only(top: 18, bottom: 18, left: 5, right: 10),
          color: Colors.blueAccent,
          child: Row(
            children: [
              Text(
                drawerNotificationItem.value,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              Spacer(),
              MouseRegion(
                cursor: SystemMouseCursors.click,
                child: GestureDetector(
                    onTap: () {
                      // call back to parent
                    },
                    child: Tooltip(
                        message: "Settings",
                        verticalOffset: 30,
                        child:
                            Icon(Icons.settings, color: Colors.white, size: 20))),
              ),
              SizedBox(
                width: 10,
              ),
              MouseRegion(
                cursor: SystemMouseCursors.click,
                child: GestureDetector(
                    onTap: () {
                      Utils.shared.pop(context);
                    },
                    child: Tooltip(
                        message: "Close",
                        verticalOffset: 30,
                        child: Icon(Icons.close, color: Colors.white, size: 20))),
              ),
            ],
          ),
        );
        break;
      case DrawerNotificationEnums.MENU_OPTIONS:
        return MouseRegion(
          cursor: SystemMouseCursors.click,
          child: Container(
            padding: EdgeInsets.only(left: 5),
            child: DropDownCustom(
              onTap: (dropDownItemSelected) {
                print(dropDownItemSelected.value);
                // call back to parent
              },
              options: getDropDownItemDemo(),
            ),
          ),
        );
        break;
    }
  }

  getDropDownItemDemo() {
    var options = <DropDownItemExample>[];
    options.add(DropDownItemExample(
        dropDownItemEnums: DropDownItemEnumsExample.DEFAULT,
        value: "All projects"));
    options.add(DropDownItemExample(
        dropDownItemEnums: DropDownItemEnumsExample.DEFAULT, value: "Mclub"));
    options.add(DropDownItemExample(
        dropDownItemEnums: DropDownItemEnumsExample.DEFAULT, value: "Carkee"));
    options.add(DropDownItemExample(
        dropDownItemEnums: DropDownItemEnumsExample.DEFAULT,
        value: "Manchester United"));
    options.add(DropDownItemExample(
        dropDownItemEnums: DropDownItemEnumsExample.DEFAULT,
        value: "Real Madrid"));

    return options;
  }
}
