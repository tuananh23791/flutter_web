import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/model/drawer_notification_item.dart';
import 'package:web_dashboard_admin/widget/drawers/drawer_notification/drawer_item_animation.dart';

class DrawerAnimationCustom extends StatefulWidget {
  final List<DrawerNotificationItemExample> options;

  const DrawerAnimationCustom({Key key, @required this.options})
      : super(key: key);

  @override
  _DrawerAnimationCustomState createState() => _DrawerAnimationCustomState();
}

class _DrawerAnimationCustomState extends State<DrawerAnimationCustom>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  double animationDuration = 0.0;
  int totalItems;

  @override
  void initState() {
    totalItems = widget.options.length;
    final int totalDuration = 3000;
    _animationController = AnimationController(
        vsync: this, duration: new Duration(milliseconds: totalDuration));
    animationDuration = totalDuration / (100 * (totalDuration / (totalItems)));
    _animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: [
        ListView.builder(
          shrinkWrap: true,
          itemCount: widget.options.length,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                DrawerItemAnimationCustom(
                  drawerNotificationItem: widget.options[index],
                  index: index,
                  animationController: _animationController,
                  duration: animationDuration,
                  onDeleteCard: (itemDeleted) {
                    setState(() {
                      widget.options.remove(itemDeleted);
                    });
                  },
                ),
              ],
            );
          },
        ),
      ],
    ));
  }
}
