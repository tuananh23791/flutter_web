import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:web_dashboard_admin/model/drawer_notification_item.dart';

class DrawerNotificationCard extends StatefulWidget {
  final DrawerNotificationItemExample itemExample;
  final Function(DrawerNotificationItemExample) onTap;
  final Function(DrawerNotificationItemExample) onDelete;

  const DrawerNotificationCard(
      {Key key,
      @required this.itemExample,
      @required this.onTap,
      @required this.onDelete})
      : super(key: key);

  @override
  _DrawerNotificationCardState createState() => _DrawerNotificationCardState();
}

class _DrawerNotificationCardState extends State<DrawerNotificationCard> {
  bool isHovered = false;

  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onHover: (event) {
        setStateIfMounted(() {
          isHovered = true;
        });
      },
      onExit: (event) {
        setStateIfMounted((){
          isHovered = false;
        });
      },
      child: Padding(
        padding: const EdgeInsets.only(right: 6.0),
        child: Card(
          elevation: isHovered ? 5 : 1,
          child: GestureDetector(
            onTap: () {
              widget.onTap(widget.itemExample);
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 6),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(5)),
              child: Stack(
                children: [
                  isHovered
                      ? Positioned(
                          top: 0,
                          right: 10,
                          child: GestureDetector(
                            onTap: () {
                              widget.onDelete(widget.itemExample);
                            },
                            child:
                                Icon(Icons.close, size: 20, color: Colors.black),
                          ))
                      : SizedBox(),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // item example
                            Text(
                              "NFCSTAR-Firebase",
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 12),
                            ),
                            isHovered
                                ? SizedBox()
                                : Text(
                                    "03:33 AM",
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Text(
                          "NFCSTAR-Firebase",
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                        SizedBox(height: 6),
                        RichText(
                          text: TextSpan(
                            children: [
                              WidgetSpan(
                                child: Icon(
                                  Icons.warning_rounded,
                                  size: 14,
                                  color: Colors.orange,
                                ),
                              ),
                              // can check by type
                              TextSpan(
                                text: "${widget.itemExample.value}",
                                style: TextStyle(
                                    color: Colors.orange,
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
