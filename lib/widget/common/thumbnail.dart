import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Thumbnail extends StatelessWidget {
  final String url;
  final double height;
  final double width;
  final Widget placeHolderWidget;
  final Widget errorWidget;
  final BoxFit fit;

  Thumbnail(this.url,
      {this.height = 200,
      this.width = 200,
      this.fit = BoxFit.cover,
      this.placeHolderWidget =
          const Center(child: const CircularProgressIndicator()),
      this.errorWidget = const Icon(Icons.error)});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: CachedNetworkImage(
        imageUrl: url,
        height: height,
        width: width,
        fit: this.fit,
        placeholder: (context, url) => this.placeHolderWidget,
        errorWidget: (context, url, error) => this.errorWidget,
      ),
    );
  }
}

class ThumbnailRatio extends StatelessWidget {
  final String url;
  final Widget placeHolderWidget;
  final Widget errorWidget;
  final BoxFit fit;
  final double aspectRatio;

  ThumbnailRatio(this.url, this.aspectRatio,
      {this.fit = BoxFit.cover,
      this.placeHolderWidget =
          const Center(child: const CircularProgressIndicator()),
      this.errorWidget = const Icon(Icons.error)});

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: CachedNetworkImage(
        imageUrl: url,
        fit: this.fit,
        placeholder: (context, url) => this.placeHolderWidget,
        errorWidget: (context, url, error) => this.errorWidget,
      ),
    );
  }
}
