import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ActionIcon extends StatefulWidget {
  final IconData icon;
  final bool enableHover;
  final Color color;
  final Color hoverColor;
  final Function onTap;

  ActionIcon(this.icon,
      {this.enableHover = false,
      this.color = Colors.black,
      this.hoverColor = Colors.black45,
      this.onTap});

  @override
  _ActionIconState createState() => _ActionIconState();
}

class _ActionIconState extends State<ActionIcon> {
  bool isHovered = false;


  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }
  _onHover(PointerEvent details){
    setStateIfMounted(() {
      isHovered = true;
    });
  }

  _onExit(PointerEvent details){
    setStateIfMounted(() {
      isHovered = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onHover: _onHover,
      onExit: _onExit,
      child: GestureDetector(
        onTap: () {
          widget.onTap();
        },
        child: Icon(
          widget.icon == null ? Icons.notifications : widget.icon,
          color: (widget.enableHover && isHovered)
              ? widget.hoverColor
              : widget.color,
        ),
      ),
    );
  }
}
