import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/size_config.dart';

import '../../constants.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    this.text,
    this.press,
    this.color = kSecondaryColor,
    this.textColor = Colors.black,
    this.enableLoading = false,
    this.showLoading = false,
    Key key,
    this.style,
  }) : super(key: key);
  final String text;
  final Function press;
  final Color color;
  final Color textColor;
  final TextStyle style;
  final bool showLoading;
  final bool enableLoading;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: color,
        onPressed: press,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                text,
                style: style ?? TextStyle(fontSize: 14, color: textColor),
              ),
              SizedBox(width: enableLoading ? 10 : 0),
              enableLoading && showLoading
                  ? SizedBox(
                      height: 16,
                      width: 16,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                          Colors.white,
                        ),
                      ),
                    )
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
