import 'package:flutter/material.dart';

class BadgeIcon extends StatelessWidget {
  final int count;
  final Icon icon;

  BadgeIcon(this.count, this.icon);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        icon == null ? Icon(Icons.notifications) : icon,
        if (count > 0)
          Positioned(
            right: 0,
            child: new Container(
              padding: EdgeInsets.all(1),
              decoration: new BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(6),
              ),
              constraints: BoxConstraints(
                minWidth: 12,
                minHeight: 12,
              ),
              child: new Text(
                '$count',
                style: new TextStyle(
                  color: Colors.white,
                  fontSize: 8,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          )
      ],
    );
  }
}
