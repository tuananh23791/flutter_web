import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/model/drop_down_item_example.dart';

class DropDownCustom extends StatefulWidget {
  final List<DropDownItemExample> options;
  final Function(DropDownItemExample) onTap;

  const DropDownCustom({Key key, @required this.options, @required this.onTap})
      : super(key: key);

  @override
  _DropDownCustomState createState() => _DropDownCustomState();
}

class _DropDownCustomState extends State<DropDownCustom> {
  String selectedItem;
  bool isHovered = false;
  Map mapping = Map<String, DropDownItemExample>();

  @override
  void initState() {
    selectedItem = widget.options[0].value;
    initMapping();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onHover: (event) {
        setState(() {
          isHovered = true;
        });
      },
      onExit: (event) {
        setState(() {
          isHovered = false;
        });
      },
      child: Container(
        child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
          onTap: () {
            if (mapping.containsKey(selectedItem)) {
              widget.onTap(mapping[selectedItem]);
            }
          },
          value: selectedItem,
          style: TextStyle(color: Colors.black, fontSize: 12),
          dropdownColor: Colors.white,
          icon: Icon(
            Icons.arrow_drop_down,
            color: isHovered ? Colors.blue : Colors.grey,
          ),
          onChanged: (String string) => setState(() => selectedItem = string),
          items: widget.options.map((DropDownItemExample item) {
            return DropdownMenuItem<String>(
              child: getWidgetByType(itemExample: item),
              value: item.value,
            );
          }).toList(),
        )),
      ),
    );
  }

  getWidgetByType({DropDownItemExample itemExample}) {
    switch (itemExample.dropDownItemEnums) {
      case DropDownItemEnumsExample.DEFAULT:
        return Text(
          '${itemExample.value}',
          style: TextStyle(
              color: isHovered && itemExample.value == selectedItem
                  ? Colors.blue
                  : Colors.black),
        );
        break;
      case DropDownItemEnumsExample.DIVIDER:
        return Divider();
        break;
      case DropDownItemEnumsExample.TITLE:
        return Divider();
        break;
    }
  }

  initMapping() {
    for (final dropDownItemExample in widget.options) {
      mapping.putIfAbsent(dropDownItemExample.value, () => dropDownItemExample);
    }
  }
}
