import 'package:flutter/material.dart';
import 'package:web_dashboard_admin/model/popup_menu_item.dart';

class PopupMenuButtonCustom extends StatefulWidget {
  final List<PopupMenuItemCustom> options;
  final Function(PopupMenuItemCustom) onItemSelected;
  final Widget icon;

  const PopupMenuButtonCustom(
      {Key key,
      @required this.options,
      @required this.onItemSelected,
      this.icon})
      : super(key: key);

  @override
  _PopupMenuButtonCustomState createState() => _PopupMenuButtonCustomState();
}

class _PopupMenuButtonCustomState extends State<PopupMenuButtonCustom> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.options == null
        ? SizedBox()
        : PopupMenuButton(
            icon: widget.icon ?? Icon(Icons.menu),
            itemBuilder: (BuildContext bc) {
              return widget.options
                  .map((item) => PopupMenuItem(
                        child:
                            getChildMenuItemByType(popupMenuItemCustom: item),
                        value: item,
                      ))
                  .toList();
            },
            onSelected: (value) {
              widget.onItemSelected(value);
            },
          );
  }

  Widget getChildMenuItemByType(
      {@required PopupMenuItemCustom popupMenuItemCustom}) {
    switch (popupMenuItemCustom.menuItemEnums) {
      case PopupMenuItemEnums.WITH_ICON:
        return Row(
          children: [
            Text(popupMenuItemCustom.value),
            Spacer(),
            Icon(
              Icons.announcement_outlined,
              color: Colors.black,
            ),
          ],
        );
      case PopupMenuItemEnums.DEFAULT:
      default:
        return Text(popupMenuItemCustom.value);
    }
  }
}
