import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/constants.dart';
import 'package:web_dashboard_admin/widget/common/default_button.dart';

import '../../size_config.dart';

class CustomDialog {
  void showAlertDialog({String message, String title}) {
    Get.dialog(
      AlertDialog(
        content: Wrap(
          children: [
            SizedBox(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  title == null
                      ? SizedBox()
                      : Text(
                          title,
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.normal),
                        ),
                  SizedBox(height: 30),
                  Text(
                    message,
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.normal),
                  ),
                  SizedBox(height: 40),
                  DefaultButton(
                    text: "OK",
                    press: () {
                      Get.back();
                    },
                    color: Colors.black,
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
