import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'dart:html' as html;

class PdfView extends StatelessWidget {
  final String url;
  final double width;
  final double height;
  PdfView({this.url ="", this.width, this.height}) {
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory('pdfView', (int viewId) {
      var pdfView = html.IFrameElement();
      pdfView.src = url;
      return pdfView;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
        width: width??400,
        height: height??300,
        child: IgnorePointer(child: HtmlElementView(viewType: 'pdfView')));
  }
}