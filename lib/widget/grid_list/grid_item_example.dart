import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:web_dashboard_admin/responsive.dart';
import 'package:web_dashboard_admin/widget/common/thumbnail.dart';

class GridItemExample extends StatefulWidget {
  final String value;
  final Function(int) onTap; // example type, enums

  const GridItemExample({Key key, this.value, this.onTap}) : super(key: key);

  @override
  _GridItemExampleState createState() => _GridItemExampleState();
}

class _GridItemExampleState extends State<GridItemExample> {
  bool _isViewHovered = false;
  bool _isEditHovered = false;
  bool _isReadMoreHovered = false;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        minHeight: 300,
        // maxHeight: 400// disable for dynamic height follow contain
      ),
      child: Card(
        elevation: 5,
        child: Container(

          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(4.0),
          ),
          child: Column(
            children: <Widget>[
              Thumbnail(
                'https://picsum.photos/200/300',
                width: double.infinity,
              ),
              SizedBox(height: 6.0),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${widget
                          .value}. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                      maxLines: Responsive.isDesktop(context)
                          ? 2
                          : Responsive.isTablet(context)
                          ? 4
                          : 6,
                      // maxLines : 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 6.0),
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(4),
                                child: MouseRegion(
                                  cursor: SystemMouseCursors.click,
                                  onHover: (event) {
                                    onHoverEvent(index: 1, isHover: true);
                                  },
                                  onExit: (event) {
                                    onHoverEvent(index: 1, isHover: false);
                                  },
                                  // onEnter: (event) {
                                  //   widget.onTap(1);
                                  // },
                                  child: Text(
                                    'View',
                                    style: TextStyle(
                                        color: _isViewHovered
                                            ? Colors.blue
                                            : Colors.black),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  border: Border(
                                    right: BorderSide(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(4),
                                child: MouseRegion(
                                  cursor: SystemMouseCursors.click,

                                  onHover: (event) {
                                    onHoverEvent(index: 2, isHover: true);
                                  },
                                  onExit: (event) {
                                    onHoverEvent(index: 2, isHover: false);
                                  },
                                  // onEnter: (event) {
                                  //   widget.onTap(2);
                                  // },
                                  child: Text(
                                      'Edit',
                                      style: TextStyle(
                                          color: _isEditHovered
                                              ? Colors.blue
                                              : Colors.black),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              width: 1,
                              color: Colors.black,
                              style: BorderStyle.solid,
                            ),
                          ),
                        ),
                        Spacer(),
                        MouseRegion(
                          cursor: SystemMouseCursors.click,
                          onHover: (event) {
                            onHoverEvent(index: 3, isHover: true);
                          },
                          onExit: (event) {
                            onHoverEvent(index: 3, isHover: false);
                          },
                          // onEnter: (event) {
                          //   widget.onTap(3);
                          // },
                          child: ElevatedButton(
                            onPressed: (){
                              print("clickeddddd");
                              widget.onTap(3);
                            },
                            child: Text('Read more',
                            style: TextStyle(
                                color: _isReadMoreHovered
                                    ? Colors.blue
                                    : Colors.black),

                          )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onHoverEvent({int index, bool isHover}) {
    setState(() {
      switch (index) {
        case 1:
          _isViewHovered = isHover;
          break;
        case 2:
          _isEditHovered = isHover;
          break;
        case 3:
          _isReadMoreHovered = isHover;
          break;
      }
    });
  }
}
