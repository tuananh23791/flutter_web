import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:web_dashboard_admin/responsive.dart';
import 'package:web_dashboard_admin/screen/details_screen.dart';
import 'package:web_dashboard_admin/widget/grid_list/grid_item_example.dart';

class GridViewCustom extends StatefulWidget {
  final List<String> options;
  final Function(String) onTap;
  final int crossAxisCount;
  final double crossAxisSpacing;
  final double mainAxisSpacing;

  const GridViewCustom({
    Key key,
    this.crossAxisCount = 1,
    this.options,
    this.onTap,
    this.crossAxisSpacing = 2.0,
    this.mainAxisSpacing = 2.0,
  }) : super(key: key);

  @override
  _GridViewCustomState createState() => _GridViewCustomState();
}

class _GridViewCustomState extends State<GridViewCustom> {
  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
      crossAxisCount: widget.crossAxisCount,
      itemCount: widget.options.length,
      itemBuilder: (BuildContext context, int index) => new GridItemExample(
        value: widget.options[index],
        onTap: (intType) {
          print("clicked GridItemExample");
          Get.to(() => DetailsScreen());
        },
      ),
      staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
      mainAxisSpacing: widget.mainAxisSpacing,
      crossAxisSpacing: widget.crossAxisSpacing,
    );
  }
}
