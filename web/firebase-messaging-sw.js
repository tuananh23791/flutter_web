importScripts("https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.4.1/firebase-messaging.js");

firebase.initializeApp({
    apiKey: "AIzaSyA5BkfwM-Bt3e5OPQch49kewgWBNDEaAa0",
    authDomain: "tony-face.firebaseapp.com",
    databaseURL: "https://tony-face.firebaseio.com",
    projectId: "tony-face",
    storageBucket: "tony-face.appspot.com",
    messagingSenderId: "847062676757",
    appId: "1:847062676757:web:6968edd0803c00f44d251c",
    measurementId: "G-6XQN2TZL94"
});

const messaging = firebase.messaging();

// Optional:
messaging.onBackgroundMessage((m) => {
  console.log("onBackgroundMessage", m);
});
messaging.setBackgroundMessageHandler(function (payload) {
    const promiseChain = clients
        .matchAll({
            type: "window",
            includeUncontrolled: true
        })
        .then(windowClients => {
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                windowClient.postMessage(payload);
            }
        })
        .then(() => {
            const title = payload.notification.title;
            const options = {
                body: payload.notification.score
              };
            return registration.showNotification(title, options);
        });
    return promiseChain;
});
self.addEventListener('notificationclick', function (event) {
    console.log('notification received: ', event)
});